<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'cliente';
    protected $fillable = ['nombre_empresa','telefono_empresa','email_empresa','rif','nombre_contacto','telefono_contacto','email_contacto','user_id','fecha_registro','observacion','eliminado','vendedor_id'];

    public function vendedor() {
        return $this->hasOne('App\User','id','vendedor_id');
    }    

}
