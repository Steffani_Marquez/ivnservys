<footer>
<!-- MAIN FOOTER-->

<!-- COPYRIGHTS                -->
    <div class="bg-dark py-5">
        <div class="container">
            <div class="row align-items-cenrer gy-3 text-center">
                <div class="col-md-6 text-md-start">
                    <p class="mb-0 text-sm text-gray-500">&copy; 2021. Your company / name goes here </p>
                </div>
                <div class="col-md-6 text text-md-end">
                    <p class="mb-0 text-sm text-gray-500">Web development  by  <a href="https://linosgo.com" target="_blank">Linos Go</a> &amp; <a href="https://hikershq.com/" target="_blank">HHQ</a> </p>
                    <p class="mb-0 text-sm text-gray-500">Template designed by  <a href="https://bootstrapious.com" target="_blank">Bootstrapious</a> &amp; <a href="https://hikershq.com/" target="_blank">HHQ</a> </p>
                    <!-- Please do not remove the backlink to us unless you purchase the Attribution-free License at https://bootstrapious.com/attribution-free-license. Thank you.-->
                </div>
            </div>
        </div>
    </div>
</footer>