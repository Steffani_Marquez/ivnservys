<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Ordenes;
use App\Documento;
use Validator;
use App\Cliente;
use Auth;
use Mail;
use App\TipoDocumento;
use App\Role;
use App\DocumentosExtra;
use Image;
use File;

class OrdersDosController extends Controller
{
    public function index()
    {
        return view('cliente.orders-02');
    }


    public function buscar(){
        $cliente = Cliente::where('user_id',Auth::user()->id)->first();
        
        
        $ordenes = Ordenes::with('permiso' ,'ci' ,'titulo' ,'planillauno' ,'planillados' ,'cliente')->where('id_cliente',$cliente->id)->where('status','!=','Anulado')->orderBy('fecha_creado','DESC')->get();
      
        return $ordenes;
    }

    public function getEliminar(Request $request){
        
        $orden = Ordenes::find($request->id);
        if($orden->status == 'Pendiente'){

        
            $planillados = Documento::find($orden->id_planilla2);
            if(!empty($planillados)){
                $docs = DocumentosExtra::where('id_documento',$planillados->id)->where('eliminado',0)->get();
                foreach($docs as $doc){
                    $file_path = public_path().'/'.$doc->url;
                    $doc->eliminado = 1;
                    $doc->save();
                    //if (File::exists($file_path)) {
                    
                    //    File::delete($file_path);
                        //unlink($file_path);
                    //}
            
                }
                if(!empty($planillados)){
                    $planillados->habilitado = 0;
                    $planillados->save();
                }
            }
            
            $orden->status = 'Anulado';
            $orden->save();
        
            //$cliente = Cliente::where('user_id',Auth::user()->id)->first();

            //$ordenes = Ordenes::with('permiso' ,'ci' ,'titulo' ,'planillauno' ,'planillados' ,'cliente')->where('id_cliente',$cliente->id)->where('status','<>','Cancelado')->orderBy('fecha_creado','DESC')->get();
        
            return 1;
        }else{
            return 0;

        }
    }    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function getCreate()
    {
        $hoy = Carbon::today('America/Caracas')->addDay()->format('d-m-Y');
        $cliente = Cliente::where('user_id',Auth::user()->id)->first();
        $desde = Carbon::now('America/Caracas')->startOfMonth();
        $hasta = Carbon::now('America/Caracas')->endOfMonth();
        
        //return $cliente->id;
        //$ordenes = Ordenes::whereBetween('fecha_creado',[$desde,$hasta])->where('id_cliente',$cliente->id)->where('status','!=','Anulado')->get();
        //$nroordenes = count($ordenes);                                                                                                           
        $planillaUno = Documento::whereBetween('fecha_registro',[$desde,$hasta])->where('id_cliente',$cliente->id)->where('tipo_documento',4)->where('habilitado',1)->orderBy('id','DESC')->first();
        //return $planillaUno;
        if(!empty($planillaUno)){
            $extraplanillaUnotodo = DocumentosExtra::where('id_documento',$planillaUno->id)->where('eliminado',0)->get();
            $extraplanillaUno = [];  
            $extraplanillaUno2 = [];  
            $i = 0;
            foreach($extraplanillaUnotodo as $permi){
                $extraplanillaUno[] = url($permi->url);
                $extraplanillaUno2[] = $permi;
                $permi->caption = $permi->nombre; 
                $i++;
                $permi->key =  $permi->id;
                if($permi->extension_doc == 'pdf'){
                    $permi->type =  'pdf';
                }else if($permi->extension_doc == 'doc' || $permi->extension_doc == 'docx' || $permi->extension_doc == 'xlx' || $permi->extension_doc == 'xlxs'){
                    $permi->type =  'office';
                }else{
                    $permi->type =  'image';
                }
                $permi->url =  url('orders-02/delete-file/'.$permi->id);

            }
        }else{
            $extraplanillaUno = "[]";
            $extraplanillaUno2 = "[]";
        }
        
        $extraplanillaUno1 = json_encode($extraplanillaUno);
        $extraplanillaUno22 = json_encode($extraplanillaUno2);
        
        return view('clientes.neworden-02',compact('hoy','extraplanillaUno1','extraplanillaUno22'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   

    public function postUploadFile(Request $request){
        $error = [];        
        $cliente = Cliente::where('user_id', Auth::user()->id)->first();
        $permiso = Documento::where('id_cliente',$cliente->id)->where('habilitado',1)->where('tipo_documento',1)->first();
        $ci = Documento::where('id_cliente',$cliente->id)->where('habilitado',1)->where('tipo_documento',2)->first();
        $titulo = Documento::where('id_cliente',$cliente->id)->where('habilitado',1)->where('tipo_documento',3)->first();
        $relacion = Documento::where('id_cliente',$cliente->id)->where('habilitado',1)->where('tipo_documento',4)->where('fecha_vencimiento','>=',Carbon::today('America/Caracas'))->first();
        $errors = '';
        $counterror = 0;
        if(empty($permiso)){
            $counterror++;                   
            $errors .= '<li>Debes Cargar el permiso, completa tu perfil o ve a la gestión de documentos y cargalo</li>';
        }

        if(empty($ci)){
            $counterror++;
            $errors .= '<li>Debes cargar la CI del Regente, completa tu perfil o ve a la gestión de documentos y cargalo</li>';
        }

        if(empty($titulo)){
            $counterror++;
            $errors .= '<li>Debes cargar el titulo del Regente, completa tu perfil o ve a la gestión de documentos y cargalo</li>';
        }
        if($request->tipo_documento == 5){
            if(empty($relacion)){
                $counterror++;
                $errors .= '<li>Debes cargar la relación de psicotrópicos del mes anterior, para poner completar la orden</li>';
            }
        }
        if ($counterror > 0) {
            //return redirect('/orders-02/create')->withErrors($error)->withInput();
            return $error;
        }

        if($request->tipo_documento == 4){
            $file = $request->file('filedoc1'); 
            $file2 = \Input::file('filedoc1'); 
        
        }

        if($request->tipo_documento == 5){
            $file = $request->file('filedoc2'); 
            $file2 = \Input::file('filedoc2'); 
        
        }

       
        $fileName = $request->fileName;          // you receive the file name as a separate post data
        $fileSize = $request->fileSize;          // you receive the file size as a separate post data
        $fileId = $request->fileId;          // you receive the file identifier as a separate post data
        $index =  $request->chunkIndex;   
        $type = $request->type;
        
        $route_photo = "uploads/";
        $error = [];
        $cliente_exist = Cliente::where('user_id',Auth::user()->id)->first();
        $imagenes = ['jpg','jpeg','JPG','JPEG','png','PNG','GIF','gif'];
        $prueba = [];
 
            //return $prueba;
            $documento = Documento::where('id_cliente',$cliente_exist->id)->where('tipo_documento',$request->tipo_documento)->where('habilitado',1)->orderBy('id','DESC')->first();
            $bandera = 0;
            if(empty($documento)){
                $bandera = 1;
            }else{
                
                if($request->tipo_documento == 5){
                    $orden = Ordenes::where('id_planilla2',$documento->id)->get();
                    if(count($orden) > 0){
                        $bandera = 1;   //existe orden asignada 
                    }else{
                        $bandera = 0;
                    }
                }else{
                    
                    //return $documento;
                    $hoy = Carbon::now('America/Caracas');
                    $vecimiento = Carbon::parse($documento->fecha_registro)->endOfMonth();
                    
                    //[$desde,$hasta]
                    //return ;
                    if($vecimiento <= $hoy){
                        $bandera = 1;                        
                    }else{
                        $bandera = 0;
                    }
                    //return $bandera;
                }
                
            }

            


            $tipo_documento = TipoDocumento::find($request->tipo_documento);
                     


            if(!empty($file2)){
           
                    $count = 1;
                    
                    
                    if($bandera == 1){
                        $documento = Documento::create([
                            'url_documento' => null
                            ,'nombre_documento' => $tipo_documento->nombre
                            ,'fecha_vencimiento' => Carbon::now('America/Caracas')->endOfMonth() 
                            ,'extension_doc' => null
                            ,'tipo_documento' => $request->tipo_documento
                            ,'id_cliente' => $cliente_exist->id
                            ,'aprobado' => 0
                            ,'habilitado' => 1
                            ,'fecha_registro' => Carbon::now('America/Caracas')
                        ]);
                    }else{
                        $documento->aprobado = 0;
                        $documento->save();
                    }                    
                    
                    //$DocumentosExtra = DocumentosExtra::where('id_documento',$documento->id)->delete();
                    
                    $prueba = [];
                    
                    foreach ($file as $f) {
                        $count = rand(5, 2);
                        $name2 = $tipo_documento->nombre_corto.$count.date('his').'.'.$f->getClientOriginalExtension();
                        $fileName = $name2;
                        $prueba[] = $name2 ;
                    
                        $extensionpermiso = $f->getClientOriginalExtension();
                        list($widthpermiso, $heightpermiso, $typepermiso, $attrpermiso) = getimagesize($f->getRealPath());
                        
        
                        if(in_array($f->getClientOriginalExtension(),$imagenes)){
                            //return $widthpermiso;
                            if($widthpermiso > 500){
                                $image_resize = Image::make($f->getRealPath());              
                                //$image_resize->resize(500, 500);
        
                                $image_resize->widen(500)->save(public_path('uploads/').$name2,60);
                            }else{
                                $carga = $f->move(public_path('uploads/'),$name2 );
                            }
                        }else{
                            
                            $carga = $f->move(public_path('uploads/'),$name2 );
                        }
                        
        
                        $permisofile = 'uploads/'.$name2;
                        $count++;

                        $targetUrl = 'https://linosgo.com/servyspsicotropicos/uploads/'.$name2;
                        $DocumentosExtra = DocumentosExtra::create([
                            'url' => $permisofile
                            ,'nombre' => $tipo_documento->nombre
                            ,'extension_doc' => $extensionpermiso
                            ,'tipo_documento' => $request->tipo_documento
                            ,'id_documento' => $documento->id
                            ,'eliminado' => 0
                        ]);
                        
                    }  
                    


                    if($extensionpermiso == 'pdf'){
                        $type =  'pdf';
                    }else if($extensionpermiso == 'doc' || $extensionpermiso == 'docx' || $extensionpermiso == 'xlx' || $extensionpermiso == 'xlxs'){
                        $type =  'office';
                    }else{
                        $type =  'image';
                    }


                    $zoomUrl = "uploads/";
                    return [
                        //'chunkIndex' => $index,         // the chunk index processed
                        'initialPreview' => $targetUrl, // the thumbnail preview data (e.g. image)
                        'initialPreviewConfig' => [
                            [
                                'id_documento' => $documento->id,
                                'tipo_documento' => $request->tipo_documento,
                                'id' => $DocumentosExtra->id,      // check previewTypes (set it to 'other' if you want no content preview)
                                'url' => "orden-02/delete-file/".$DocumentosExtra->id,      // check previewTypes (set it to 'other' if you want no content preview)
                                'type' => $type,      // check previewTypes (set it to 'other' if you want no content preview)
                                'caption' => $tipo_documento->nombre, // caption
                                'key' => $DocumentosExtra->id,       // keys for deleting/reorganizing preview
                                'fileId' => $DocumentosExtra->id,    // file identifier
                                'size' => $fileSize,    // file size
                                'zoomData' => $targetUrl, // separate larger zoom data
                            ]
                        ],
                        'append' => true
                    ];                

            }else{
                if(empty($documentopermiso)){
                    return 'El permiso es obligatorio';
                }
            }    
            

    }
    public function getEmailNotifica($emailscliente,$titulo,$mensaje,$mensaje2){
        
        $data = [
            'titulo' => $titulo,
            'mensaje' => $mensaje,
            'mensaje2' => $mensaje2
            ];
        
            $message = 'prueba';

            $role = Role::where('name','REGENTE')->first();
            foreach($role->users as $user){
                $emails[] = $user->email;
            }
             
            foreach($emailscliente as $email){
                $emails[] = $email;
            }
             
            $emails[] = 'marquez.steffani@gmail.com';
            $emails[] = 'agomez.servys@gmail.com';
            
            return  Mail::send('email.notificaciones', $data, function($message) use ($emails,$titulo)
            {
                
                $message->from('no-reply@linosgo.com', 'Droguería Servys');
                
                $message->to($emails)->subject($titulo);
                
            });



    }

    public function postDeleteFile($key){
        //return $key;
        $doc = DocumentosExtra::find($key);

        //$file_path = public_path().'/'.$doc->url;
        //return $file_path ;
        //if (File::exists($file_path)) {
            
        //    File::delete($file_path);
            //unlink($file_path);
        //}
        $documento = Documento::find($doc->id_documento);
        //$estatus = ['Anulado','Cancelado'];
        $estatus = ['Facturado','Cancelado','Anulado'];
        $orden = Ordenes::where('id_planilla1',$documento->id)->whereNotIn('status',$estatus)->get();
        if(count($orden) > 0){
            return 'Tienes un pedido pendiente con esta relación. Ve a tus ordenes y cancelalo.';
        }
        $doc->eliminado = 1;
        $doc->save();
        $documento->habilitado = 0;
        $documento->save();
        //$doc->delete();
        
        
        return $doc;
    }

    public function postHasOrden(Request $request){
        
        $doc = DocumentosExtra::find($request->key);

        $documento = Documento::find($doc->id_documento);
        $estatus = ['Facturado','Cancelado','Anulado'];
        $permiso = Ordenes::where('id_permiso',$documento->id)->whereNotIn('status',$estatus)->get();
        
        if(count($permiso) > 0){
            return 'Tienes un pedido pendiente con este Permiso. Ve a tus ordenes y cancelalo.';
        }
        
        return 0;
    }

    public function postSaveOrden(Request $request){
        $error = [];     
        $cliente = Cliente::where('user_id', Auth::user()->id)->first();
        $permiso = Documento::where('id_cliente',$cliente->id)->where('habilitado',1)->where('tipo_documento',1)->orderBy('fecha_registro','DESC')->first();
        $ci = Documento::where('id_cliente',$cliente->id)->where('habilitado',1)->where('tipo_documento',2)->orderBy('fecha_registro','DESC')->first();
        $titulo = Documento::where('id_cliente',$cliente->id)->where('habilitado',1)->where('tipo_documento',3)->orderBy('fecha_registro','DESC')->first();
        $relacion = Documento::where('id_cliente',$cliente->id)->where('habilitado',1)->where('tipo_documento',4)->orderBy('fecha_registro','DESC')->first();
        $actualizacion = Documento::where('id_cliente',$cliente->id)->where('habilitado',1)->where('tipo_documento',7)->orderBy('fecha_registro','DESC')->first();
        $cambior = Documento::where('id_cliente',$cliente->id)->where('habilitado',1)->where('tipo_documento',8)->orderBy('fecha_registro','DESC')->first();
        if(empty($permiso)){
            $error[] = 'Debes Cargar el permiso, completa tu perfil o ve a la gestión de documentos y cargalo';
        }

        if(empty($ci)){
            $error[] = 'Debes cargar la CI del Regente, completa tu perfil o ve a la gestión de documentos y cargalo';
        }

        if(empty($titulo)){
            $error[] = 'Debes cargar el titulo del Regente, completa tu perfil o ve a la gestión de documentos y cargalo';
        }
        if($request->tipo_documento == 5){
            if(empty($relacion)){
                $error[] = 'Debes cargar la relación de psicotrópicos del mes anterior, para poner completar la orden';
            }
        }
        if (count($error) > 0) {
            //return redirect('/orders-02/create')->withErrors($error)->withInput();
            return $error;
        }

        if($request->tipo_documento == 5){
            $ordenes = Ordenes::create([
                'id_permiso' => $permiso->id
                ,'id_ciregente' => $ci->id
                ,'id_titulo' => $titulo->id
                ,'id_planilla1' => $relacion->id
                ,'id_planilla2' => $request->id_documento
                ,'id_actualizacion' => !empty($actualizacion->id) ? $actualizacion->id : null
                ,'id_cambior' => !empty($cambior->id) ? $cambior->id : null
                ,'fecha_pedido' => Carbon::parse($request->fecha_pedido)->format('Y-m-d')
                ,'fecha_creado'=> Carbon::now('America/Caracas')
                ,'status' => 'Pendiente'
                ,'id_cliente' => $cliente->id
            ]);
            if(!empty($cliente->vendedor->email)){
                //return $cliente->vendedor->email;
                $titulo  = 'NUEVO PEDIDO';
                $mensaje = 'Te notificamos que hay una nueva solicitud de psicotropicos del cliente '.$cliente->nombre_empresa;
                $mensaje2 = '';
                self::getEmailNotifica([$cliente->vendedor->email],$titulo,$mensaje,$mensaje2);
            }    
            return "Orden Creada exitosamente";

        }
    }

}
