      <!-- Navbar Sticky-->
      <header class="nav-holder make-sticky">
        <div class="navbar navbar-light bg-white navbar-expand-lg py-0" id="navbar">
          <div class="container py-3 py-lg-0 px-lg-0">
            <!-- Navbar brand--><a class="navbar-brand" href="/"><img class="d-none d-md-inline-block" src="img/logo.png" alt="Servys logo" style="width: 50%;"><img class="d-inline-block d-md-none" src="img/logo.png" alt="Universal logo"><span class="sr-only">Servys</span></a>
            <!-- Navbar toggler-->
            <button class="navbar-toggler text-primary border-primary" type="button" data-bs-toggle="collapse" data-bs-target="#navigationCollapse" aria-controls="navigationCollapse" aria-expanded="false" aria-label="Toggle navigation"><span class="sr-only">Toggle navigation</span><i class="fas fa-align-justify"></i></button>
            <!-- Collapsed Navigation    -->
            <div class="collapse navbar-collapse" id="navigationCollapse">
              <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <!-- homepage dropdown-->
                <li class="nav-item"><a class="nav-link" id="hpDropdown" href="img/pro.xlsx" role="button" aria-expanded="true">Lista Productos</a>
                </li>
                <li class="nav-item"><a class="nav-link" id="hpDropdown" href="orders-02" role="button" aria-expanded="true">Mis pedidos</a>
                </li>
                <li class="nav-item"><a class="nav-link" id="hpDropdown" href="compra" role="button" aria-expanded="true">Nuevo Pedido</a>
                </li>

                <li class="nav-item"><a class="nav-link" id="hpDropdown" href="/drogueria-servys"  role="button" aria-expanded="true">Salir</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>