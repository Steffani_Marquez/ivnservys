

$(document).on('click', '.kv-file-zoom', function () {
    
    $('#kvFileinputModal').removeClass('fade');
    
});



$(document).ready(function() {
    
    $('#tipo_documento').change(function(){
        if($(this).val() == 1){
            $('.fecha_vencimiento').removeAttr('hidden');
        }else{
            $('.fecha_vencimiento').attr('hidden','hidden');
        }
        tipo_documento = $('#tipo_documento').val(); 
    });

    if($('#extradocpermiso').val() == '[]'){
        datos = false;
        datosarray = false;
    }else{
        datos = JSON.parse($('#extradocpermiso').val());
        datosarray = JSON.parse($('#extradocpermiso2').val());
    }
    
    if($('#extradocci1').val() == '[]'){
        datos2 = false;
        datosarray2 = false;
    }else{
        datos2 = JSON.parse($('#extradocci1').val());
        datosarray2 = JSON.parse($('#extradocci2').val());
    }

    if($('#extradoctitulo1').val() == '[]'){
        datos3 = false;
        datosarray3 = false;
    }else{
        datos3 = JSON.parse($('#extradoctitulo1').val());
        datosarray3 = JSON.parse($('#extradoctitulo2').val());
    }
    
    if($('#extplanillauno1').val() == '[]'){
        datos4 = false;
        datosarray4 = false;
    }else{
        datos4 = JSON.parse($('#extplanillauno1').val());
        datosarray4 = JSON.parse($('#extplanillauno2').val());
    }
    
    
    if($('#extactualizacion1').val() == '"[]"'){
        
        $('.actualizacion').css('display','none');
        datos7 = false;
        datosarray7 = false;
    }else{
        
        datos7 = JSON.parse($('#extactualizacion1').val());
        datosarray7 = JSON.parse($('#extactualizacion2').val());
    }

    if($('#extcambior1').val() == '"[]"'){
        
        $('.cambior').css('display','none');
        datos8 = false;
        datosarray8 = false;
    }else{
        
        datos8 = JSON.parse($('#extcambior1').val());
        datosarray8 = JSON.parse($('#extcambior2').val());
    }

    if($('#fecha_vencimiento').val() == ''){
         fechaVencimiento = '';
    }else{
         fechaVencimiento = $('#fecha_vencimiento').val();

    }
                     
   
    $("#archivo1").fileinput({
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos == "[]") ? false : datos,
        //initialPreview: [
        //    'https://linosgo.com/servyspsicotropicos/uploads/4085223.pdf'
        //],
        //initialPreviewConfig: [
        //    {type: 'pdf', description: "<h5>PDF File One</h5> This is a representative placeholder description number one for this PDF file.", size: 2050}
        //],
        initialPreviewConfig:  (datos == "[]") ? false : datosarray, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:false,
        showRemove:false,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image' || config.type === 'office') {
                    return true;
                }
                return false;
            }
        }
    });

    $("#archivo2").fileinput({
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos2 == "[]") ? false : datos2,
        //initialPreview: [
        //    'https://linosgo.com/servyspsicotropicos/uploads/4085223.pdf'
        //],
        //initialPreviewConfig: [
        //    {type: 'pdf', description: "<h5>PDF File One</h5> This is a representative placeholder description number one for this PDF file.", size: 2050}
        //],
        initialPreviewConfig:  (datos2 == "[]") ? false : datosarray2, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:false,
        showRemove:false,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image' || config.type === 'office') {
                    return true;
                }
                return false;
            }
        }
    });
     
    $("#archivo3").fileinput({
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos3 == "[]") ? false : datos3,
        //initialPreview: [
        //    'https://linosgo.com/servyspsicotropicos/uploads/4085223.pdf'
        //],
        //initialPreviewConfig: [
        //    {type: 'pdf', description: "<h5>PDF File One</h5> This is a representative placeholder description number one for this PDF file.", size: 2050}
        //],
        initialPreviewConfig:  (datos3 == "[]") ? false : datosarray3, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:false,
        showRemove:false,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image' || config.type === 'office') {
                    return true;
                }
                return false;
            }
        }
    });  
      
    $("#archivo4").fileinput({
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos4 == "[]") ? false : datos4,
        //initialPreview: [
        //    'https://linosgo.com/servyspsicotropicos/uploads/4085223.pdf'
        //],
        //initialPreviewConfig: [
        //    {type: 'pdf', description: "<h5>PDF File One</h5> This is a representative placeholder description number one for this PDF file.", size: 2050}
        //],
        initialPreviewConfig:  (datos4 == "[]") ? false : datosarray4, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:false,
        showRemove:false,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image' || config.type === 'office') {
                    return true;
                }
                return false;
            }
        }
    });  
      
    $("#archivo7").fileinput({
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos7 == "[]") ? false : datos7,
        //initialPreview: [
        //    'https://linosgo.com/servyspsicotropicos/uploads/4085223.pdf'
        //],
        //initialPreviewConfig: [
        //    {type: 'pdf', description: "<h5>PDF File One</h5> This is a representative placeholder description number one for this PDF file.", size: 2050}
        //],
        initialPreviewConfig:  (datos7 == "[]") ? false : datosarray7, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:false,
        showRemove:false,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image' || config.type === 'office') {
                    return true;
                }
                return false;
            }
        }
    });     
    
    $("#archivo8").fileinput({
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos8 == "[]") ? false : datos8,
        //initialPreview: [
        //    'https://linosgo.com/servyspsicotropicos/uploads/4085223.pdf'
        //],
        //initialPreviewConfig: [
        //    {type: 'pdf', description: "<h5>PDF File One</h5> This is a representative placeholder description number one for this PDF file.", size: 2050}
        //],
        initialPreviewConfig:  (datos8 == "[]") ? false : datosarray8, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:false,
        showRemove:false,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image' || config.type === 'office') {
                    return true;
                }
                return false;
            }
        }
    });     
       
});
