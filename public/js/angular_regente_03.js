		
		var AllPost_app = angular.module('ordenesApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('ordenesController', function($scope, $http) {

 
			$scope.orden = [];
			$http.get($('#orden_id').val()+'/buscar', {params: { id: $('#orden_id').val()}}).
			success(function(data, status, headers, config){
			$scope.orden = data;
            console.log($scope.orden);
			});

			$scope.buscar = function(status){
                    
				$scope.orden = [];
				$http.get($('#orden_id').val()+'/buscar', {params: {  id: $('#orden_id').val() }}).
				success(function(data, status, headers, config){
				$scope.orden = data;
				});
			}
			/*
			$scope.openmodaldetalle = function(orden){
                
                $('#observacion').val('');
                $('#openmodaldetalle').show();
				$scope.orden = orden;
                console.log(orden);
			}					
            */
        
            $scope.aprobar = function(elemento){
                 
                $('#boton'+elemento).addClass('btn-success');
                let aprobado = 0;
                if($('#'+elemento+'aprobado').val() == '0'){
                    aprobado = 1;
                    if(elemento == 'permiso'){
                        $scope.orden.permiso.aprobado = 1;
                    }
                    
                    if(elemento == 'ci'){
                        $scope.orden.ci.aprobado = 1;
                    }
                    
                    if(elemento == 'titulo'){
                        $scope.orden.titulo.aprobado = 1;
                    }
                    
                    if(elemento == 'planillauno'){
                        $scope.orden.planillauno.aprobado = 1;
                    }
                    
                    if(elemento == 'planillados'){
                        $scope.orden.planillados.aprobado = 1;
                    }
                }
                if($('#'+elemento+'aprobado').val() == '1'){
                    aprobado = 0;
                    $('#boton'+elemento).removeClass('btn-success');
                    $('#boton'+elemento).addClass('btn-default');
                    if(elemento == 'permiso'){
                        $scope.orden.permiso.aprobado = 0;
                    }
                    
                    if(elemento == 'ci'){
                        $scope.orden.ci.aprobado = 0;
                    }
                    
                    if(elemento == 'titulo'){
                        $scope.orden.titulo.aprobado = 0;
                    }
                    
                    if(elemento == 'planillauno'){
                        $scope.orden.planillauno.aprobado = 0;
                    }
                    
                    if(elemento == 'planillados'){
                        $scope.orden.planillados.aprobado = 0;
                    }
                }
                
                $http.get($('#orden_id').val()+'/aprobaciondocumentos', {params: { id_orden : $('#orden_id').val(), documento_id: $('#'+elemento+'_id').val(), aprobado: aprobado  }}).
				success(function(data, status, headers, config){
				    //$scope.ordenes_arr = data;
                    $scope.buscar(); 
    				$('#openmodaldetalle').modal('toggle');
                    alert('Cambio Exitoso');

				});  
            }
            
            $scope.guardar_observacion = function(){
                console.log($('#observacion').val());
                $http.post($('#orden_id').val()+'/guardar-observacion', { id_orden : $('#orden_id').val(), observacion: $('#observacion').val()  }).
				success(function(data, status, headers, config){
				    //$scope.ordenes_arr = data;
                    $scope.buscar(); 
    				$('#openmodaldetalle').modal('toggle');
                    alert('Observación enviada Exitosamente');

				});
            }

	});