		
		var AllPost_app = angular.module('ordenesApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('ordenesController', function($scope, $http) {

 
			$scope.clientes_arr = [];
			$http.get('revision-clientes-02/buscar', {params: { nombre:1}}).
			success(function(data, status, headers, config){
			$scope.clientes_arr = data;
			});

			$scope.buscar = function(){

				$scope.clientes_arr = [];
				$http.get('revision-clientes-02/buscar', {params: { nombre: 1 }}).
				success(function(data, status, headers, config){
				$scope.clientes_arr = data;
				});
			}
			
			$scope.openmodaldetalle = function(cliente){
				$('#observacion').val('');
				$('#openmodaldetalle').modal('toggle');
				
				$scope.clientedocumentos = cliente.documentos;
				$scope.cliente = cliente;
                console.log($scope.clientedocumentos);
			}					

        
            $scope.aprobar = function(elemento,id){
                
                
                let aprobado = 0;
                if($('#'+elemento+'aprobado').val() == '0'){
                    $('#boton'+elemento).addClass('btn-success');
                    $scope.clientedocumentos[elemento].aprobado = 1;
                    aprobado = 1;
                }

                if($('#'+elemento+'aprobado').val() == '1'){
                    $('#boton'+elemento).removeClass('btn-success');
                    $('#boton'+elemento).addClass('btn-default');
                    $scope.clientedocumentos[elemento].aprobado = 0;
                    aprobado = 0;
                }

                $http.get('revision-clientes-02/aprobaciondocumentos', {params: { id_documento : id, aprobado: aprobado }}).
				success(function(data, status, headers, config){
				    //$scope.clientes_arr = data;
                    alert('Cambio Exitoso');
				});                

            }
            
			$scope.guardarobservacion = function(id){
                $http.get('revision-clientes-02/guarda-observacion', {params: { id_cliente : id, observacion: $('#observacion').val() }}).
				success(function(data, status, headers, config){
					if(data == 0){
						$('.alerta-error').text('Este cliente no tiene correo electronico asociado');
						$('.alerta-error').removeAttr('hidden','false');
					}else{
						$('#openmodaldetalle').modal('toggle');

					}

				});				
			}

			$scope.openmodalenviarcodigo = function(cliente){
				$scope.cliente = cliente;
				$('#openmodalenviarcodigo').modal('toggle');

			}
			
			$scope.guardarcodigo = function(id){
                $http.get('revision-clientes-02/guardar-codigo', {params: { id_cliente : id, codigo: $('#codigo').val() }}).
				success(function(data, status, headers, config){
					$scope.cliente.codigo.codigo = data.codigo;
					$('#openmodalenviarcodigo').modal('toggle');
					$('.alerta').text('Código Enviado Correctamente');
		
					$('.alerta').removeAttr('hidden','false')

				});				
			}	

			$scope.openmodalasigarvendedor = function(cliente){
				$scope.cliente = cliente;

				$('#openmodalasigarvendedor').modal('toggle');

			}

			$scope.asignarVendedor = function(id){
                $http.get('revision-clientes-02/asignar-vendedor', {params: { id_cliente : id, vendedor_id: $('#vendedor_id').val() }}).
				success(function(data, status, headers, config){
					$scope.cliente.nombre_vendedor = data;
					$('#openmodalasigarvendedor').modal('toggle');
					$('.alerta').text('Vendedor Asignado Correctamente');
					$('.alerta').removeAttr('hidden','false')

				});				
			}	
			
			
			$scope.openmodaleliminar = function(cliente){
				$scope.cliente = cliente;
				$('#openmodaleliminar').modal('toggle');

			}
			
			$scope.eliminar = function(id){
                $http.get('revision-clientes-02/eliminar', {params: { id_cliente : id }}).
				success(function(data, status, headers, config){
					$scope.buscar();
					$('.alerta').text('Eliminado Correctamente');
					$('#openmodaleliminar').modal('toggle');
					$('.alerta').removeAttr('hidden','false')
					

				});				
			}

			$scope.eliminarPermanente = function(id){
                $http.get('revision-clientes-02/eliminar-permatente', {params: { id_cliente : id }}).
				success(function(data, status, headers, config){
					$scope.buscar();
					$('.alerta').text('Eliminado Permanente');
					$('#openmodaleliminar').modal('toggle');
					$('.alerta').removeAttr('hidden','false')

				});				
			}

			$scope.openmodaleitar = function(cliente){
				$('#openmodaleitar').modal('toggle');
				$scope.cliente = cliente;
			}
			
			$scope.editarcliente = function(id){
                $http.get('revision-clientes-02/editar', {params: { id_cliente : id, empresa_name: $('#empresa-name').val(),  nombre_contact: $('#nombre-contact').val(),phone_contact: $('#phone-contact').val(), empresa_rif: $('#empresa-rif').val(), email_contact : $('#email-contact').val() }}).
				success(function(data, status, headers, config){
					$scope.buscar();
					$('.alerta-success').text('Editado correctamente');
					$('#openmodaleitar').modal('toggle');
					$('.alerta-success').removeAttr('hidden','false')
				});				
			}
			
	});