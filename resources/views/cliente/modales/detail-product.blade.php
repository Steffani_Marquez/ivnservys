<div class="modal" id="verproductomodal" aria-modal="true" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="product">
                <div class="product-image"><img alt="" class="img-fluid " id="img" src="img/productos/producto1.png" ></div>
                <div class='text-center'>
                    <h4 class="mt-2 mb-2 ng-binding" id="nombre">07/2022</h4>
                    <h5 class="mt-2 mb-2 ng-binding" id="fv">07/2022</h5>
                    <h5 class="mb-2 price ng-binding" id="precio">1,08 $</h5>
                    
                </div>
                <div>
                    <p id="vademecum">
                    Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500
                    </p>
                </div>
                <div class="carverde" style="display: none;" id="checkcar_car"><img src="img/carrito.svg" class="carrits"> Producto actualizado</div>
                <div class="carverde" style="display: none;" id="checkcar_car"><img src="img/carrito.svg" class="carrits"> Producto eliminado</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" aria-label="Close">Volver</button>
            </div>
        </div>
    </div>
</div>