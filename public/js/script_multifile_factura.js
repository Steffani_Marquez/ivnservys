

$(document).on('click', '.kv-file-zoom', function () {
    
    $('#kvFileinputModal').removeClass('fade');
    
});



$(document).ready(function() {
    

    if($('#extfactura1').val() == '[]'){
        datos6 = false;
        datosarray6 = false;
    }else{
        datos6 = JSON.parse($('#extfactura1').val());
        datosarray6 = JSON.parse($('#extfactura2').val());
    }

       

    $("#archivo6").fileinput({
        uploadUrl: "/servyspsicotropicos/enviar-factura/"+$('#orden_id').val()+"/upload-file",
        enableResumableUpload: false,
        uploadExtraData: {
            'tipo_documento': 6, // for access control / security 
        },
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos6 == "[]") ? false : datos6,
        initialPreviewConfig:  (datos6 == "[]") ? false : datosarray6, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:true,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            }
        }
        /*
        layoutTemplates: {
            actions: `
            <button type="button" class="kv-file-check btn btn-sm btn-kv btn-default btn-outline-secondary" title="check"><i class="fas fa-check"></i></button>
            `    
        }
        */
    });           
});
