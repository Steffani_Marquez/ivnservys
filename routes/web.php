<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/compra', 'CategoriaProductosController@index')->name('/compra');
Route::get('/carrito', 'CategoriaProductosController@carrito')->name('/carrito');
Route::get('/mis-ordenes', 'CategoriaProductosController@ordenes')->name('/mis-ordenes');
Route::get('/detalle-orden', 'CategoriaProductosController@detalle_orden')->name('/detalle-orden');
Route::post('/login', 'HomeController@login')->name('/login');

Route::get('/orders-02', 'OrdersDosController@index')->name('/orders-02');
Route::get('/orders-02/buscar', 'OrdersDosController@buscar')->name('/orders-02/buscar');
Route::get('/orders-02/eliminar', 'OrdersDosController@eliminar')->name('/orders-02/eliminar');
Route::get('/orden-detalle-cliente','OrdenDetalleClienteController@index')->name('/orden-detalle-cliente'); 
Route::get('/logout', 'Auth\AuthController@getLogout')->name('/logout'); 
Route::get('/', 'HomeController@index')->name('/');
