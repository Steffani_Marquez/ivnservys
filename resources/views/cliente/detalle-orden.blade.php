@extends('cliente.cliente-layout')
@section('content')
@section('title', 'Detalle pedido #'. $id )
@section('enlace1', 'Mis pedidos')
@section('url1', 'orders-02')
@section('enlace2', 'Detalle pedido')
<style>
  .kv-file-remove{
    display: none;
  }
  .btn-file{
    display: none;
  }
  .file-caption{
    display: none;
  }
  .file-drop-zone-title{
    display: none;
  }
</style>
<section id="main-content" ng-module="ordenesApp" ng-controller="ordenesController">
    <section class="container">
        <div class="">

    
            <div class="loading"></div>
                    
            @if(Session('success'))
                <div class="alert alert-success">
                    {{Session('success')}}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" data-bs-target="#my-alert" aria-label="Close"></button>

                </div>
            @endif
            @if(count($errors) > 0)
            <div class="alert alert-danger d-flex align-items-center">
                <button type="button" class="btn-close" data-bs-dismiss="alert" data-bs-target="#my-alert" aria-label="Close"></button>
                <strong>Error!</strong>
                <ul>
                    @foreach ($errors->all() as $error) 
                        <li>{{$error}}</li>
                    @endforeach
                </ul> 
            </div>
            @endif
            <input type="hidden" id="extradocpermiso" value="{{$ext1}}">
            <input type="hidden" id="extradocpermiso2" value="{{$ext2}}">
            <input type="hidden" id="extradocci1" value="{{$extci1}}">
            <input type="hidden" id="extradocci2" value="{{$extci2}}">
            <input type="hidden" id="extradoctitulo1" value="{{$exttitulo1}}">
            <input type="hidden" id="extradoctitulo2" value="{{$exttitulo2}}">

            <input type="hidden" id="extplanillauno1" value="{{$extplanillauno1}}">
            <input type="hidden" id="extplanillauno2" value="{{$extplanillauno2}}">
            <input type="hidden" id="extplanillados1" value="{{$extplanillados1}}">
            <input type="hidden" id="extplanillados2" value="{{$extplanillados2}}">
            <input type="hidden" id="extfactura1" value="{{$extfacturauno1}}">
            <input type="hidden" id="extfactura2" value="{{$extfacturados2}}">

            <input type="hidden" id="extactualizacion1" value="{{$extalizacionuno1}}">
            <input type="hidden" id="extactualizacion2" value="{{$extalizaciondos2}}">
            <input type="hidden" id="extcambior1" value="{{$extacambior1}}">
            <input type="hidden" id="extcambior2" value="{{$extacambior2}}">

            <input type="hidden" id="orden_id" value="{{$id}}">
            <hr>  
            <p class="lead mb-4">La orden fue recibida <strong>31/01/2022</strong> y esta siendo <strong>procesada</strong>.</p>
            <p class="lead text-muted mb-5">Si tiene una inquietud contacte a su asesor de venta</p>
            <div class="row mt" >   
                <div class="col-md-12">
                    <h3 ><b> Permiso de funcionamiento Vigente</b> </h3>

                    <span ng-if="orden.permiso.aprobado == 0">
                        <h4 class="text-danger"><strong>Sin Aprobar</strong> </h4>
                    </span>
                    <span ng-if="orden.permiso.aprobado == 1">
                        <h4 class="text-success"><strong>Aprobado</strong>  </h4>
                    </span>
                    <input type="hidden" name="permisoaprobado" id="permisoaprobado" value="<% orden.permiso.aprobado %>">
                    <input type="hidden" name="permiso_id" id="permiso_id" value="<% orden.permiso.id %>">
                </div>
                <div class="col-md-12">
                    <div class="col-md-4 upxs fecha_vencimiento" >
                        <label><strong>Fecha de Vencimiento :</strong> <span> {{$fecha_vencimiento}} </span></label>                                  
                    </div>                                
                </div>
                <div class="col-md-12">                             
                    <div class="col-md-12">                                    
                        <input type="file" multiple  class="ifilepermiso file" name="filepermiso[]" id="archivo1" value=""/>
                    </div>
                </div>
            </div>
            <h3 style="margin-bottom: 25px;">Datos del Regente</h3>
            <div class="row mt" >                   
                <div class="col-md-12">      
                    <h4><b> C.I:</b> </h4>
                    <span ng-if="orden.ci.aprobado == 0">
                        <h4 class="text-danger"><strong>Sin Aprobar</strong> </h4>
                    </span>
                    <span ng-if="orden.ci.aprobado == 1">
                        <h4 class="text-success"><strong>Aprobado</strong>  </h4>
                    </span>
                    <input type="hidden" name="ciaprobado" id="ciaprobado" value="<% orden.ci.aprobado %>">
                    <input type="hidden" name="ci_id" id="ci_id" value="<% orden.ci.id %>">
                </div>
                
                <div class="col-md-12">                                    
                    <input type="file" multiple  class="ifilepermiso file" name="fileci[]" id="archivo2" value=""/>
                </div>                                
            </div>
            <div class="row mt">
                <div class="col-md-12">      
                    <h4><b> Título</b> </h4>
                    <span ng-if="orden.titulo.aprobado == 0">
                        <h4 class="text-danger"><strong>Sin Aprobar</strong> </h4>
                    </span>
                    <span ng-if="orden.titulo.aprobado == 1">
                        <h4 class="text-success"><strong>Aprobado</strong>  </h4>
                    </span>
                    <input type="hidden" name="tituloaprobado" id="tituloaprobado" value="<% orden.titulo.aprobado %>">
                    <input type="hidden" name="titulo_id" id="titulo_id" value="<% orden.titulo.id %>">
                </div>
                <div class="col-md-12">                                    
                    <input type="file" multiple  class="ifilepermiso file" name="filetitulo[]" id="archivo3" value=""/>
                </div>
            </div>   
            <div class="row mt" >
                <div class="col-md-12">      
                    <h4><b>Actualización</b> </h4>
                    <span class="row" ng-if="orden.actualizacion.aprobado == 0">
                        <h4 class="text-danger"><strong>Sin Aprobar</strong> </h4>
                    </span>
                    <span ng-if="orden.actualizacion.aprobado == 1">
                        <h4 class="text-success"><strong>Aprobado</strong>  </h4>
                    </span>
                    <input type="hidden" name="actualizacionaprobado" id="actualizacionaprobado" value="<% orden.actualizacion.aprobado %>">
                    <input type="hidden" name="actualizacion_id" id="actualizacion_id" value="<% orden.actualizacion.id %>">
                </div>
                <div class="col-md-12">                                    
                    <input type="file" multiple  class="ifilepermiso file" name="fileplanillados[]" id="archivo7" value=""/>
                </div>
            </div>               
            <div class="row mt" >
                <div class="col-md-12">      
                    <h4><b> Cambio de regente</b> </h4>
                    <span class="row" ng-if="orden.cambior.aprobado == 0">
                        <h4 class="text-danger"><strong>Sin Aprobar</strong> </h4>
                    </span>
                    <span ng-if="orden.cambior.aprobado == 1">
                        <h4 class="text-success"><strong>Aprobado</strong>  </h4>
                    </span>
                    <input type="hidden" name="cambioraprobado" id="cambioraprobado" value="<% orden.cambior.aprobado %>">
                    <input type="hidden" name="cambior_id" id="cambior_id" value="<% orden.cambior.id %>">
                </div>
                <div class="col-md-12">                                    
                    <input type="file" multiple  class="ifilepermiso file" name="filecambior[]" id="archivo8" value=""/>
                </div>
            </div>                
            <div class="row mt">
                <div class="col-md-12">      
                    <h4><b> Relación del mes anterior</b> </h4>
                    <span ng-if="orden.planillauno.aprobado == 0">
                        <h4 class="text-danger"><strong>Sin Aprobar</strong> </h4>
                    </span>
                    <span ng-if="orden.planillauno.aprobado == 1">
                        <h4 class="text-success"><strong>Aprobado</strong>  </h4>
                    </span>
                    <input type="hidden" name="planillaunoaprobado" id="planillaunoaprobado" value="<% orden.planillauno.aprobado %>">
                    <input type="hidden" name="planillauno_id" id="planillauno_id" value="<% orden.planillauno.id %>">
                </div>
                <div class="col-md-12">                                    
                    <input type="file" multiple  class="ifilepermiso file" name="fileplanillauno[]" id="archivo4" value=""/>
                </div>
            </div> 
            <div class="row mt">
                <div class="col-md-12">      
                    <h4><b> Planilla de Solicitud</b> </h4>
                    <span class="row" ng-if="orden.planillados.aprobado == 0">
                        <h4 class="text-danger"><strong>Sin Aprobar</strong> </h4>
                    </span>
                    <span ng-if="orden.planillados.aprobado == 1">
                        <h4 class="text-success"><strong>Aprobado</strong>  </h4>
                    </span>
                    <input type="hidden" name="planilladosaprobado" id="planilladosaprobado" value="<% orden.planillados.aprobado %>">
                    <input type="hidden" name="planillados_id" id="planillados_id" value="<% orden.planillados.id %>">
                </div>
                <div class="col-md-12">                                    
                    <input type="file" multiple  class="ifilepermiso file" name="fileplanillados[]" id="archivo5" value=""/>
                </div>
            </div>             
            <div class="row mt">
                <div class="col-md-12">                                    
                <h4><b>Factura</b> </h4>
                    <input type="file" multiple  class="ifilepermiso file" name="filefactura[]" id="archivo6" value=""/>
                </div>
            </div>
            <div class="row mt1">

                <div class="col-lg-12">
                <h3 ><b> Resumen de la Compra</b> </h3>
                    <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                        <tr class="text-sm">
                            <th class="border-gray-300 border-top py-3" >Producto</th>
                            <th class="border-gray-300 border-top py-3">Nombre</th>
                            <th class="border-gray-300 border-top py-3">Cantidad</th>
                            <th class="border-gray-300 border-top py-3">Precio unitario</th>
                            <th class="border-gray-300 border-top py-3">Total</th>
                            <th class="border-gray-300 border-top py-3"></th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!empty($datos))
                            @foreach($datos as $dato)
                            <tr class="text-sm">
                                <td class="align-middle border-gray-300 py-3"><img class="img-fluid flex-shrink-0" src="{{$dato->url}}"  alt="White Blouse Armani" style="min-width: 50px" width="50"></td>
                                <td class="align-middle border-gray-300 py-3">{{$dato->nombre}}</td>
                                <td class="align-middle border-gray-300 py-3">{{$dato->cantidad}}</td>
                                <td class="align-middle border-gray-300 py-3">$10</td>
                                <td class="align-middle border-gray-300 py-3">$<span class="stotal">{{$dato->cantidad * 10}}</span></td>
                                <td class="align-middle border-gray-300 py-3"></td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th class="text-end lead py-3" colspan="5">Subtotal</th>
                            <th class="lead py-3 totalgenerral">$</th>
                        </tr>
                        <tr>
                            <th class="text-end lead py-3" colspan="5">Días de crédito</th>
                            <th class="lead py-3">3</th>
                        </tr>
                        <tr>
                            <th class="border-0 text-end lead py-3" colspan="5">Total</th>
                            <th class="border-0 lead py-3 totalgenerral">$</th>
                        </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    <!-- /wrapper -->
</section>

@stop
<script src="{{url('/js/lib/jquery/jquery.min.js')}}"></script>
<script src="{{url('/js/script_multifile_ordenes_clientes.js')}}"></script>
@section('script')

<script src="{{url('/js/angular_detalle_cliente.js')}}"></script>
@stop