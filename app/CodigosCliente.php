<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodigosCliente extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'codigos_clientes';
    protected $fillable = ['codigo','id_cliente','fecha_registro','mes'];

    public function cliente() {
        return $this->hasOne('App\Cliente','id','id_cliente');
    }    

}
