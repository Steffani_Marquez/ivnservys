	
		var AllPost_app = angular.module('DetallePedidoApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('DetallePedidoController', function($scope, $http) {
    
            $scope.trx;
            $http.get('/detalle-pedido/'+$('#trx_id').val()+'/pedido',{params: {id_trx:$('#trx_id').val() }}).
            success(function(data, status, headers, config){
                $scope.trx = data;
            });
            
            $scope.cambiar_status = function(id) { 
                $('.modal_status').modal('toggle');
            }
            $scope.cambiar_status_pago = function(id) { 
                $('.modal_status_pago').modal('toggle'); 
            }
            $scope.eliminaritemmodal = function(id,tipo) { 
                $('.modal_eliminar_item').modal('toggle'); 
                $('#pedido_vendido_id').val(id);
                $('#pedido_vendido_tipo').val(tipo);
                
            } 
            $scope.agregaritemmodal = function() { 
                $('.mensaje_alerta').text('');
                $('.modal_agrega_item').modal('toggle'); 
            }
            
            
            $scope.editando_pagos_status = function(){
                $http.get('/pedidos/changestatuspagos',{params: {id_trx:$('#trx_id').val(), status_id: $('#status_pago').val() }}).
				success(function(data, status, headers, config){
                    $scope.trx.status_pago.color_rgb = data.color_rgb;
                    $scope.trx.status_pago.status = data.status_name;
                    $scope.trx.status_pago.id = data.status_pago_id;
                    $('.modal_status_pago').modal('hide');
                    
				});
            }
            
            $scope.editando_status = function(){
                $http.get('/pedidos/changestatus',{params: {id_trx: $('#trx_id').val(), status_id: $('#status').val() }}).
				success(function(data, status, headers, config){
                    $scope.trx.status.color_rgb = data.color_rgb;
                    $scope.trx.status.status = data.status_name;
                    $scope.trx.status.id = data.status_id;
                    $('.modal_status').modal('hide');
                    
				});
            }

            $scope.eliminar_item = function(id){
                $http.get('/pedidos/eliminando-item',{params: {id_trx: $('#trx_id').val(),id: $('#pedido_vendido_id').val(),tipo: $('#pedido_vendido_tipo').val() }}).
				success(function(data, status, headers, config){
                    $('.modal_eliminar_item').modal('hide');
                    $scope.trx = data;
				});
            }           

            $scope.agregar_item = function(id){
                
                if($('#producto_id').val() == ""){ 
                    $('.mensaje_alerta').css('display','block');
                    $('.mensaje_alerta').text('Debes seleccionar el producto');
                    return false;
                }
                if($('#cantidad').val() == '0'){ 
                    $('.mensaje_alerta').text('Debes ingresar la cantidad del producto');
                    $('.mensaje_alerta').css('display','block');
                    return false;
                }

                $('.mensaje_alerta').css('display','none');
                $http.get('/pedidos/agregar-item',{params: {id_trx: $('#trx_id').val(),id_producto: $('#producto_id').val(),cantidad: $('#cantidad').val() }}).
				success(function(data, status, headers, config){
                    $('.modal_agrega_item').modal('hide');
                    $scope.trx = data;
				});
            }            
            

		});