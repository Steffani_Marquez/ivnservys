		
		var AllPost_app = angular.module('ordenesApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('ordenesController', function($scope, $http) {

 
			$scope.ordenes_arr = [];
			$http.get('ordenes-vendedor/buscar', {params: { status:'Pendiente'}}).
			success(function(data, status, headers, config){
			$scope.ordenes_arr = data;
			});

			$scope.buscar = function(status){

				$scope.ordenes_arr = [];
				$http.get('ordenes-vendedor/buscar', {params: { status: status }}).
				success(function(data, status, headers, config){
				$scope.ordenes_arr = data;
				});
			}
			
			$scope.openmodaldetalle = function(orden){
				$('#openmodaldetalle').modal('toggle');
				$scope.orden = orden;
                console.log(orden);
			}					


	});