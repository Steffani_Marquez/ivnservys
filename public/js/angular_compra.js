		
		var AllPost_app = angular.module('compraApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('compraController', function($scope, $http) {

 
			$scope.productos = [];
			$http.get('js/productos.json', {params: { nombre:1}}).
			success(function(data, status, headers, config){
				console.log(data)
			$scope.productos = data;
			});

			$scope.buscar = function(){
				console.log('aqui')
				$scope.productos = [];
				$http.get('js/productos.json', {params: { nombre: 1 }}).
				success(function(data, status, headers, config){
				console.log(data);
					$scope.productos = data;
				});
			}
			
        			
			$scope.openmodal = function(producto){

				$scope.producto = producto;
				$('#verproductomodal').modal('toggle');
				$('#img').attr('src',producto.url);
				$('#precio').text(producto.precio);
				$('#nombre').text(producto.nombre);
				$('#vademecum').html(producto.vademecum);
				$('#fv').text(producto.fv);
			}			
            var totalcarris = 0;
			var data =  new Array();

			
			$scope.restarppv_car =function (id) { 

			valor = $("#inputvalor"+id).val();

			if (valor == 0) {
				return false;
			}

			nuevovalor = parseFloat(valor) - parseFloat(1);
			$("#inputvalor"+id).val(nuevovalor);
					
			}

			$scope.sumarppv_car =function (id) { 
			valor = $("#inputvalor"+id).val();
			nuevovalor = parseFloat(valor) + parseFloat(1);
			$("#inputvalor"+id).val(nuevovalor);
			}

			$scope.agregaralcarr = function (id) { 
								
				valor = $("#inputvalor"+id).val();
					if (valor == 0) {
							return false;
				}

				data.push({    
					"id" : id,    
					"url": $(`.data-url${id}`).attr('src'), 
					"nombre" : $(`.nombre${id}`).text(),
					"cantidad"  : valor 
				});

				$("#load"+id).show();
				$("#cajacarris"+id).hide();
				$("#checkcar"+id).show();
				
			
				$("#load"+id).hide();
				$("#cajacarris"+id).show();
				totalcarris += Number(valor);
				$("#totalcarris").text(totalcarris);
					
				//});

				valor = $("#inputvalor"+id).val(0);

				setTimeout(function() {
				$("#checkcar"+id).fadeOut(2000);
				},1000);

			}

			$scope.enviaCarrito = function (){
			datos = JSON.stringify(data)
			window.location = 'carrito?datos='+datos;
			}

	});