		
		var AllPost_app = angular.module('CantinaPromosionesApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});
        
        AllPost_app.controller('CantinaPromosionesController', function($scope, $http) {
            
 
					$scope.promociones_arr = [];
					$http.get('promociones/buscar', {params: { nombre:1}}).
					success(function(data, status, headers, config){
					    $scope.promociones_arr = data;
                      
					});

                    $scope.nuevopromociones = function(){
                       
                        $('#nuevopromociones').modal('toggle');
                    }
                    var count = 0;
                    $scope.nuevoclonproducto = function(){
                        var source = $('.formclone:first'),
                        clone = source.clone();
            
                        clone.find(':input').attr('id', function(i, val) {
                            return val + count;
                        });
                        
                        clone.insertBefore(this);
                        
                        count++;
                    }
                    $scope.productospro = [];
                    $scope.editarpromociones = function(promo){

                        $('#nombre_edicion').val(promo.nombre);
                        $('#precio_edicion').val(promo.costo);
                        $('#descripcion_edicion').text(promo.descripcion);
                        $scope.productospro = promo.productos;
                        console.log($scope.productospro);
                        $scope.promo_edicon = promo;
                        $('.productosselect:first').val('');
                        $('.cantidadinput:first').val('');
                        $('#editarpromociones').modal('toggle');
                    }
       

           
                    
                    $scope.nuevo = function() {
                        var formData = new FormData(document.getElementById("nuevopromocionform"));
                        console.log(formData);
                        //formData.append("dato", "valor");
                        //formData.append(f.attr("name"), $(this)[0].files[0]);
                        datos= [];
                        $.ajax({
                            url: "promociones/nuevo",
                            type: "post",
                            dataType: "html",
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false
                        }).done(function(res){

                            $http.get('promociones/buscar', {params: { nombre:1}}).
                            success(function(data, status, headers, config){
                            $scope.promociones_arr = data;
                            });
    
                            //$scope.promociones_arr.push(res);
                            $('#nuevopromociones').modal('toggle');
                        });
           
					}

                    $scope.editar = function() {
                        var formData = new FormData(document.getElementById("editarpromocionform"));
                        console.log(formData);
                        //formData.append("dato", "valor");
                        //formData.append(f.attr("name"), $(this)[0].files[0]);
                        $.ajax({
                            url: "promociones/editar",
                            type: "post",
                            dataType: "html",
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false
                        }).done(function(res){

                            $http.get('promociones/buscar', {params: { nombre:1}}).
                            success(function(data, status, headers, config){
                            $scope.promociones_arr = data;
                            });
    
                            //$scope.promociones_arr.push(res);
                            $('#editarpromociones').modal('toggle');
                        });

					}

                    
                    $scope.tipoPromoChange = function(){
                        console.log($('#tipo_promo').val());
                        if($('#tipo_promo').val() == 'descuento'){
                            $('.condicion').attr('hidden','hidden');
                            $('.condiciondos').removeAttr('hidden');
                        }else if($('#tipo_promo').val() == 'volumen'){
                            $('.condicion').removeAttr('hidden');
                            $('.condiciondos').attr('hidden','hidden');
                        }
                    }

                                        
                    $scope.tipoPromoChangeEdicion = function(){
                        console.log($('#tipo_promo_edicion').val());
                        if($('#tipo_promo_edicion').val() == 'descuento'){
                            $('.condicion').attr('hidden','hidden');
                            
                        }else if($('#tipo_promo_edicion').val() == 'volumen'){
                            $('.condicion').removeAttr('hidden');
                            
                        }
                    }
                    

	});