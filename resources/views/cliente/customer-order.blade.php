@extends('cliente.cliente-layout')
@section('title', 'Detalle orden')
@section('enlace1', 'Home')
@section('enlace2', 'Detalle orden')
@section('content')
<section class="py-5">
    <div class="container py-4">
        <div class="row gy-5">
            <div class="col-lg-12">
                <p class="lead mb-4">La orden #1735 fue recibida <strong>31/01/2022</strong> y esta siendo <strong>procesada</strong>.</p>
                <p class="lead text-muted mb-5">Si tiene una inquietud contacte a su asesor de venta</p>
                <div class="table-responsive">
                <table class="table text-nowrap">
                    <thead>
                    <tr class="text-sm">
                        <th class="border-gray-300 border-top py-3" >Producto</th>
                        <th class="border-gray-300 border-top py-3">Nombre</th>
                        <th class="border-gray-300 border-top py-3">Cantidad</th>
                        <th class="border-gray-300 border-top py-3">Precio unitario</th>
                        <th class="border-gray-300 border-top py-3">Total</th>
                        <th class="border-gray-300 border-top py-3"></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($datos as $dato)
                        <tr class="text-sm">
                            <td class="align-middle border-gray-300 py-3"><img class="img-fluid flex-shrink-0" src="{{$dato->url}}"  alt="White Blouse Armani" style="min-width: 50px" width="50"></td>
                            <td class="align-middle border-gray-300 py-3">{{$dato->nombre}}</td>
                            <td class="align-middle border-gray-300 py-3">{{$dato->cantidad}}</td>
                            <td class="align-middle border-gray-300 py-3">$10</td>
                            <td class="align-middle border-gray-300 py-3">$<span class="stotal">{{$dato->cantidad * 10}}</span></td>
                            <td class="align-middle border-gray-300 py-3"></td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th class="text-end lead py-3" colspan="5">Subtotal</th>
                        <th class="lead py-3 totalgenerral">$</th>
                    </tr>
                    <tr>
                        <th class="text-end lead py-3" colspan="5">Días de crédito</th>
                        <th class="lead py-3">3</th>
                    </tr>
                    <tr>
                        <th class="border-0 text-end lead py-3" colspan="5">Total</th>
                        <th class="border-0 lead py-3 totalgenerral">$</th>
                    </tr>
                    </tfoot>
                </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    function total(){
    var totalgenerral = 0;
    $(".stotal").each(function(){
        totalgenerral += Number($(this).text())
    });
    $(".totalgenerral").text(`$${totalgenerral}`);
}
total()
</script>
@endsection