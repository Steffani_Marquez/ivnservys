	
		var AllPost_app = angular.module('AdsalvavidasApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('SalvavidasController', function($scope, $http) {
			

				$http.get('/datos-completed').
				success(function(data, status, headers, config){

					if(data == 0){
						//alert('dfsdf');
						//console.log(data);
						$('#modaldatos').modal('toggle');
					}
				});
				
				$scope.openmodalsicm = function(){
					$('#modaldatos').modal('toggle');
				}
				

				$scope.buscarsicm = function(){
					
					let datobusqueda = $('#datobusqueda').val();
					
					$scope.direcciones = [];
					if(datobusqueda != ''){

						$scope.direcciones = [];
						$http.get('/obtener-contribuyente', {params: { dato: datobusqueda }}).
						success(function(data, status, headers, config){
							$scope.direcciones = data;
							$('.direcciones').removeAttr('hidden');
						});
					}else{
						$('.direcciones').attr('hidden','hidden');
						alert('DEBES INGRESAR EL CÓDIGO SICM O RIF PARA VERIFICAR LOS DATOS DE ENVIO');
					}
				}

				$scope.guarda_direcciones = function(){
					
					$http.get('/insert-direction', {params: {  'direcciones_id': JSON.stringify($('[name="direcciones[]"]').serializeArray()) }}).
						success(function(data, status, headers, config){

							if(data != 1){
								$scope.sicmexisten = data;
								$('.alertexist').text('');
								
								$('.alertexist').removeAttr('hidden');
								$('.alertexist').append('<div>SICM ya se encuentran registrados</div>');
								$('.alertexist').append('<ul>');
								data.forEach(value => $('.alertexist').append('<li>'+value+'</li>'));
								$('.alertexist').append('</ul>');
								
							}else{
								$('#modaldatos').modal('hide');
							}
					});
				}

				$scope.guarda_direccion_manual = function(){
					
					let bandera = 0;
					if($('#direccion').val() == ''){
						$('.alertvalidacion').removeAttr('hidden');
						$('.alertvalidacion').text('El Debes ingresar el Dirección');
						bandera = 1;
					}

					if($('#tipo_sicm').val() == ''){
						$('.alertvalidacion').removeAttr('hidden');
						$('.alertvalidacion').text('El Debes Seleccionar un Tipo de SICM');
						bandera = 1;
					}

					if($('#status').val() == ''){
						$('.alertvalidacion').removeAttr('hidden');
						$('.alertvalidacion').text('El Debes Seleccionar un Estatus');
						bandera = 1;
					}

					if($('#rif').val() == ''){
						$('.alertvalidacion').removeAttr('hidden');
						$('.alertvalidacion').text('El Debes ingresar el RIF');
					}

					if($('#codsicm').val() == ''){
						$('.alertvalidacion').removeAttr('hidden');
						$('.alertvalidacion').text('El Debes ingresar el código SICM');
						bandera = 1;
					}					
					

					
					if(bandera == 0 ){
						$('.alertvalidacion').attr('hidden','hidden');				
						$http.get('/insert-direction-manual', {params: {  'codsicm': $('#codsicm').val(), 'rif': $('#rif').val(), 'namefiscal': $('#namefiscal').val(), 'status': $('#status').val(), 'tipo_sicm': $('#tipo_sicm').val(), 'direccion': $('#direccion').val() }}).
							success(function(data, status, headers, config){

								if(data != 1){
									$scope.sicmexisten = data;
									$('.alertexist').text('');
									
									$('.alertexist').removeAttr('hidden');
									$('.alertexist').append('<div>SICM ya se encuentra registrado</div>');
									
								}else{
									$('#modaldatos').modal('hide');
								}
								
						});
					}
				}

				$scope.productos_arr = [];
				$http.get('/buscarproductos').
				success(function(data, status, headers, config){
				$scope.productos_arr = data;
				});


				$http.get('/buscarcarrito').
				success(function(data, status, headers, config){
				   $("#totalcarris").text(data);
				});



				$scope.productos_recomen_arr = [];
				$http.get('/buscarproductosrecomendados').
				success(function(data, status, headers, config){
				$scope.productos_recomen_arr = data;
				});

				$scope.restarppv = function(id,tipo) { 
				   
				   valor = $("#inputvalor"+id+tipo).val();

				   if (valor == 0) {
                      return false;
				   }

				   nuevovalor = parseFloat(valor) - parseFloat(1);
				   $("#inputvalor"+id+tipo).val(nuevovalor);
                     
				}

				$scope.sumarppv = function(id,tipo) { 
					
					console.log(tipo);
                    valor = $("#inputvalor"+id+tipo).val();
					
					nuevovalor = parseFloat(valor) + parseFloat(1);
					$("#inputvalor"+id+tipo).val(nuevovalor);
				}

				$scope.agregaralcarr = function(id,tipo) { 
					
					valor = $("#inputvalor"+id+tipo).val();
					

					if (valor == 0) {
                      return false;
				   }

                    $("#load"+id+tipo).show();
                    $("#cajacarris"+id+tipo).hide();
                    $("#checkcar"+id+tipo).show();
				   	
					$http.get('/guardarcarrito', {params: { id: id,cantidad:valor,tipo:tipo }}).
					success(function(data, status, headers, config){
						if(data == 'registro'){
							window.location = '/registro';
						}
						$("#load"+id+tipo).hide();
						$("#cajacarris"+id+tipo).show();

						$("#totalcarris").text(data);
						  
					});

					valor = $("#inputvalor"+id+tipo).val(0);

					setTimeout(function() {
	                  $("#checkcar"+id+tipo).fadeOut(2000);
	                },1000);



				}

				$scope.verproducto = function(prod){
					
					$scope.image = prod.image;
					$scope.description = prod.description;
					$scope.precio_maximo = prod.precio_maximo_formateado;
					$('#verproductomodal').modal('toggle');
				}

		});