<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDocumento extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'tipo_documento';
    protected $fillable = ['nombre','nombre_corto'];

}
