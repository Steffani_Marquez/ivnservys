		
		var AllPost_app = angular.module('ordenesApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('ordenesController', function($scope, $http) {



			$scope.clientes_arr = [];
			$http.get($('#cliente_id').val()+'/buscar', {params: { id_cliente : $('#cliente_id').val()}}).
			success(function(data, status, headers, config){
			$scope.cliente = data;
			});

			$http.get($('#cliente_id').val()+'/observaciones', {params: { id_cliente : $('#cliente_id').val()}}).
			success(function(data, status, headers, config){
			$scope.observaciones_arr = data;
			});
			$scope.buscar = function(){

				$http.get($('#cliente_id').val()+'/buscar', {params: { id_cliente : $('#cliente_id').val()}}).
				success(function(data, status, headers, config){
				$scope.cliente = data;
				});
			}
			
        
            $scope.aprobar = function(elemento){
                 
                $('#boton'+elemento).addClass('btn-success');
                let aprobado = 0;
                if($('#'+elemento+'aprobado').val() == '0'){
                    aprobado = 1;
                    if(elemento == 'permiso'){
                        $scope.cliente.permiso.aprobado = 1;
                    }
                    
                    if(elemento == 'ci'){
                        $scope.cliente.ci.aprobado = 1;
                    }
                    
                    if(elemento == 'titulo'){
                        $scope.cliente.titulo.aprobado = 1;
                    }
                    
                    if(elemento == 'planillauno'){
                        $scope.cliente.planillauno.aprobado = 1;
                    }
                    
                    if(elemento == 'planillados'){
                        $scope.cliente.planillados.aprobado = 1;
                    }
                }
                if($('#'+elemento+'aprobado').val() == '1'){
                    aprobado = 0;
                    $('#boton'+elemento).removeClass('btn-success');
                    $('#boton'+elemento).addClass('btn-default');
                    if(elemento == 'permiso'){
                        $scope.cliente.permiso.aprobado = 0;
                    }
                    
                    if(elemento == 'ci'){
                        $scope.cliente.ci.aprobado = 0;
                    }
                    
                    if(elemento == 'titulo'){
                        $scope.cliente.titulo.aprobado = 0;
                    }
                    
                    if(elemento == 'planillauno'){
                        $scope.cliente.planillauno.aprobado = 0;
                    }
                    
              
                }
                
                $http.get($('#cliente_id').val()+'/aprobaciondocumentos', {params: { id_cliente : $('#cliente_id').val(), documento_id: $('#'+elemento+'_id').val(), aprobado: aprobado  }}).
				success(function(data, status, headers, config){
                    
                    alert('Cambio Exitoso');
					$scope.buscar(); 

				});  
            }
            
			$scope.guardar_observacion = function(){
                $http.post($('#cliente_id').val()+'/guardar-observacion', { id_cliente : $('#cliente_id').val(), observacion: $('#observacion').val()  }).
				success(function(data, status, headers, config){
				    //$scope.ordenes_arr = data;
                    $scope.buscar(); 
    				alert('Observación enviada Exitosamente');

				});
            }
	
			$scope.guardarcodigo = function(){
                $http.get($('#cliente_id').val()+'/guardar-codigo', {params: { id_cliente :$('#cliente_id').val(), codigo: $('#codigo').val() }}).
				success(function(data, status, headers, config){
					$scope.cliente.codigo = data.codigo;
					alert('Código Enviado Correctamente');

				});				
			}	

			$scope.asignarVendedor = function(id){
                $http.get($('#cliente_id').val()+'/asignar-vendedor', {params: { id_cliente : $('#cliente_id').val(), vendedor_id: $('#vendedor_id').val() }}).
				success(function(data, status, headers, config){
					$scope.cliente.nombre_vendedor = data;
					alert('Vendedor Asignado Correctamente');

				});				
			}	
			
			
			$scope.openmodaleliminar = function(cliente){
				$scope.cliente = cliente;
				$('#openmodaleliminar').modal('toggle');

			}
			
			$scope.eliminar = function(id){
                $http.get('revision-clientes/eliminar', {params: { id_cliente : id }}).
				success(function(data, status, headers, config){
					$scope.buscar();
					$('.alerta').text('Eliminado Correctamente');
					$('#openmodaleliminar').modal('toggle');
					$('.alerta').removeAttr('hidden','false')
					

				});				
			}

			$scope.eliminarPermanente = function(id){
                $http.get('revision-clientes/eliminar-permatente', {params: { id_cliente : id }}).
				success(function(data, status, headers, config){
					$scope.buscar();
					$('.alerta').text('Eliminado Permanente');
					$('#openmodaleliminar').modal('toggle');
					$('.alerta').removeAttr('hidden','false')

				});				
			}


			$scope.editarcliente = function(){
                $http.get($('#cliente_id').val()+'/editar', {params: { id_cliente :$('#cliente_id').val(), empresa_name: $('#empresa-name').val(),  nombre_contact: $('#nombre-contact').val(),phone_contact: $('#phone-contact').val(), empresa_rif: $('#empresa-rif').val(), email_contact : $('#email-contact').val() }}).
				success(function(data, status, headers, config){
					
					alert('Editado correctamente');
					$scope.buscar();
					
				});				
			}
			
	});