<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use Auth;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login(Request $request)
    {
      

      
        $username = $request->username;
        //return $request;
        $user = User::whereUsername($username)->first();
         
        if(empty($user)){
            return 0;
        }
        
        if($user->active === 0) {
            return 0;
        }

        $password = $request->password;
        if ( $user && Hash::check($password ,$user->password)){
            
        //    return Hash::check($password ,$user->password) ? 'si': 'no';
            
            Auth::loginUsingId($user->id,true);
            
            if (Auth::check()) {
                $user = Auth::user();
                $auth = $user->roles()->first();
                
                switch ($auth->name) {
                        case 'CLIENTE':
                            //$usuario = Usuario::where('user_id',$user->id)->first();
                            return  "orders-02";    
                        break;
                        case 'SUPERADMIN':
                            //$usuario = Usuario::where('user_id',$user->id)->first();
                            return 'dashboard-02';  
                        break;
                        case 'REGENTE':
                            //$usuario = Usuario::where('user_id',$user->id)->first();
                            return 'revision-ordenes-02';  
                        break;
                        case 'FACTURACION':
                            //$usuario = Usuario::where('user_id',$user->id)->first();
                            //return  redirect('pedidoscusica?f400k22ksjd2e='.$usuario->id.'&ff='.$fecha1.'&k=19'); 
                            return 'control-facturacion-02'; 
                        break;
                        case 'VENDEDOR':
                            //$usuario = Usuario::where('user_id',$user->id)->first();
                            //return  redirect('pedidoscusica?f400k22ksjd2e='.$usuario->id.'&ff='.$fecha1.'&k=19'); 
                            return 'ordenes-vendedor-02'; 
                        break;
                    //Eduardo Zapata 18589568 eduardo@gmail.com
                    //Franklin Olivero 17641441 franklin@gmail.com
                    } 
            }
        }else{
            return 0;
        }    
    }
}
