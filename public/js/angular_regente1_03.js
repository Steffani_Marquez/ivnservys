		
		var AllPost_app = angular.module('ordenesApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('ordenesController', function($scope, $http) {

 
			$scope.ordenes_arr = [];
			$http.get('revision-ordenes-02/buscar', {params: { status:'Pendiente', desde: $('#desde').val() , hasta: $('#hasta').val()}}).
			success(function(data, status, headers, config){
			$scope.ordenes_arr = data;
			});

			$scope.buscar = function(status){
                

				$scope.ordenes_arr = [];
				$http.get('revision-ordenes-02/buscar', {params: { status: status, desde: $('#desde').val() , hasta: $('#hasta').val() }}).
				success(function(data, status, headers, config){
				$scope.ordenes_arr = data;
				});
			}
			
			$scope.openmodaldetalle = function(orden){
                
                $('#observacion').val('');
                $('#openmodaldetalle').show();
				$scope.orden = orden;
                console.log(orden);
			}					

        
            $scope.aprobar = function(elemento){
                 
                $('#boton'+elemento).addClass('btn-success');
                
                if($('#'+elemento+'aprobado').val() == '0'){

                    if(elemento == 'permiso'){
                        $scope.orden.permiso.aprobado = 1;
                    }
                    
                    if(elemento == 'ci'){
                        $scope.orden.ci.aprobado = 1;
                    }
                    
                    if(elemento == 'titulo'){
                        $scope.orden.titulo.aprobado = 1;
                    }
                    
                    if(elemento == 'planillauno'){
                        $scope.orden.planillauno.aprobado = 1;
                    }
                    
                    if(elemento == 'planillados'){
                        $scope.orden.planillados.aprobado = 1;
                    }
                }
                if($('#'+elemento+'aprobado').val() == '1'){
                    $('#boton'+elemento).removeClass('btn-success');
                    $('#boton'+elemento).addClass('btn-default');
                    if(elemento == 'permiso'){
                        $scope.orden.permiso.aprobado = 0;
                    }
                    
                    if(elemento == 'ci'){
                        $scope.orden.ci.aprobado = 0;
                    }
                    
                    if(elemento == 'titulo'){
                        $scope.orden.titulo.aprobado = 0;
                    }
                    
                    if(elemento == 'planillauno'){
                        $scope.orden.planillauno.aprobado = 0;
                    }
                    
                    if(elemento == 'planillados'){
                        $scope.orden.planillados.aprobado = 0;
                    }
                }

            }
            
            $scope.guardaraprobacion = function(id){
                $http.get('revision-ordenes-02/aprobaciondocumentos', {params: { id_orden : id, permisoaprobado: $('#permisoaprobado').val(), ciaprobado: $('#ciaprobado').val(), tituloaprobado: $('#tituloaprobado').val() ,planillaunoaprobado: $('#planillaunoaprobado').val() ,planilladosaprobado: $('#planilladosaprobado').val(), observacion: $('#observacion').val() }}).
				success(function(data, status, headers, config){
				    //$scope.ordenes_arr = data;
                    $scope.buscar(); 
    				$('#openmodaldetalle').modal('toggle');
                    alert('Cambio Exitoso');

				});                
            }

	});