	
		var AllPost_app = angular.module('ClientesApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('ClientesController', function($scope, $http) {
    
           
            $scope.autorizar = function(id){
                $http.get('/cliente/autorizar',{params: {id_direccion : id }}).
				success(function(data, status, headers, config){
                    $scope.autorizado = data; 

                    if(data.autorizado == 0){
                        $('.color'+id).css('color','#dc3545');
                    }
                    if(data.autorizado == 1){
                        $('.color'+id).css('color','#28a745');
                        
                    }
                    
                    
                    
				});
            }
            
            $scope.asignarvendedor = function(id){
                $http.get('/cliente/asignarvendedor',{params: {cliente_id : id, vendedor_id: $('#vendedor').val() }}).
				success(function(data, status, headers, config){
                    $scope.vendedor = data.vendedor; 
                    
				});
            }
  
            $http.get('/cliente/vendedorasignado',{params: {cliente_id:$('#cliente_id').val()}}).
            success(function(data, status, headers, config){
                $scope.vendedor = data.vendedor;
            });
            $http.get('/cliente/direcciones',{params: {cliente_id:$('#cliente_id').val()}}).
				success(function(data, status, headers, config){
                    $scope.direcciones = data;
            });
            
		});