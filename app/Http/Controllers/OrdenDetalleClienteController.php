<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ordenes;
use App\Cliente;
use App\DocumentosExtra;
use App\Documento;
use App\Observaciones;
use Mail;
use App\Role;
use Carbon\Carbon;
use Auth;

class OrdenDetalleClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id= $request->id;
        if(!empty($request->datos)){
            $datos = json_decode($request->datos); 

        }else{
            $datos = json_decode([]); 

        }

        $orden = Ordenes::find($id);
        
        $id2 = $orden->id;
        if(!empty($orden->permiso->id)){
            $extrapermisotodo = DocumentosExtra::where('id_documento',$orden->permiso->id)->where('eliminado',0)->get();
            $extrapermiso = [];  
            $extrapermiso2 = [];  
            $i = 0;
            foreach($extrapermisotodo as $permi){
                $extrapermiso[] = url('img/planilla.docx');
                $i++;
                if($permi->extension_doc == 'pdf'){
                    $permi->type =  'pdf';
                }else if($permi->extension_doc == 'doc' || $permi->extension_doc == 'docx' || $permi->extension_doc == 'xlx' || $permi->extension_doc == 'xlxs'){
                    $permi->type =  'office';
                }else{
                    $permi->type =  'image';
                }
                
                $new_array['extension_doc'] =  $permi->extension_doc; 
                $new_array['caption'] =  $permi->nombre.'.'.$permi->extension_doc;
                $new_array['key'] =  $permi->id;
                $new_array['type'] =  'office';
                $new_array['deleteUrl'] =  false;
                $new_array['downloadUrl'] =   url('img/planilla.docx');
                
                $extrapermiso2[] = $new_array;
                break;

            }
        }else{
            $extrapermiso = "[]";
            $extrapermiso2 = "[]";
        }

        if(!empty($orden->ci->id)){
            $extracitodo = DocumentosExtra::where('id_documento',$orden->ci->id)->where('eliminado',0)->get();
            $extraci = [];  
            $extraci2 = [];  
            $i = 0;
            foreach($extracitodo as $permi){
                $extraci[] = url('img/planilla.docx');
                $i++;
                if($permi->extension_doc == 'pdf'){
                    $permi->type =  'pdf';
                }else if($permi->extension_doc == 'doc' || $permi->extension_doc == 'docx' || $permi->extension_doc == 'xlx' || $permi->extension_doc == 'xlxs'){
                    $permi->type =  'office';
                }else{
                    $permi->type =  'image';
                }
                
                $new_array['extension_doc'] =  $permi->extension_doc; 
                $new_array['caption'] =  $permi->nombre.'.'.$permi->extension_doc;
                $new_array['key'] =  $permi->id;
                $new_array['type'] =  'office';
                $new_array['deleteUrl'] =  false;
                $new_array['downloadUrl'] = url('img/planilla.docx');
                
                $extraci2[] = $new_array;
                
                break;
            }
        }else{
            $extraci = "[]";
            $extraci2 = "[]";
        }

        if(!empty($orden->titulo->id)){
            $extratitulotodo = DocumentosExtra::where('id_documento',$orden->titulo->id)->where('eliminado',0)->get();
            $extratitulo = [];  
            $extratitulo2 = [];  
            $i = 0;
            foreach($extratitulotodo as $permi){
                $extratitulo[] = url('img/planilla.docx');
                $i++;
                if($permi->extension_doc == 'pdf'){
                    $permi->type =  'pdf';
                }else if($permi->extension_doc == 'doc' || $permi->extension_doc == 'docx' || $permi->extension_doc == 'xlx' || $permi->extension_doc == 'xlxs'){
                    $permi->type =  'office';
                }else{
                    $permi->type =  'image';
                }
                
                $new_array['extension_doc'] =  $permi->extension_doc; 
                $new_array['caption'] =  $permi->nombre.'.'.$permi->extension_doc;
                $new_array['key'] =  $permi->id;
                $new_array['type'] =  'office';
                $new_array['deleteUrl'] =  false;
                $new_array['downloadUrl'] =   url('img/planilla.docx');
                
                $extratitulo2[] = $new_array;
                
                break;
            }
        }else{
            $extratitulo = "[]";
            $extratitulo2 = "[]";
        }

        if(!empty($orden->planillauno->id)){
            $extraplanillaunotodo = DocumentosExtra::where('id_documento',$orden->planillauno->id)->where('eliminado',0)->get();
            $extraplanillauno = [];  
            $extraplanillauno2 = [];  
            $i = 0;
            foreach($extraplanillaunotodo as $permi){
                $extraplanillauno[] = url('img/planilla.docx');
                $i++;
                if($permi->extension_doc == 'pdf'){
                    $permi->type =  'pdf';
                }else if($permi->extension_doc == 'doc' || $permi->extension_doc == 'docx' || $permi->extension_doc == 'xlx' || $permi->extension_doc == 'xlxs'){
                    $permi->type =  'office';
                }else{
                    $permi->type =  'image';
                }
                
                $new_array['extension_doc'] =  $permi->extension_doc; 
                $new_array['caption'] =  $permi->nombre.'.'.$permi->extension_doc;
                $new_array['key'] =  $permi->id;
                $new_array['type'] =  'office';
                $new_array['deleteUrl'] =  false;
                $new_array['downloadUrl'] =   url('img/planilla.docx');
                
                $extraplanillauno2[] = $new_array;
                
                break;
            }
        }else{
            $extraplanillauno = "[]";
            $extraplanillauno2 = "[]";
        }

        if(!empty($orden->planillados->id)){
            $extraplanilladostodo = DocumentosExtra::where('id_documento',$orden->planillados->id)->where('eliminado',0)->get();
            $extraplanillados = [];  
            $extraplanillados2 = [];  
            $i = 0;
            foreach($extraplanilladostodo as $permi){
                $extraplanillados[] = url('img/planilla.docx');
                $i++;
                if($permi->extension_doc == 'pdf'){
                    $permi->type =  'pdf';
                }else if($permi->extension_doc == 'doc' || $permi->extension_doc == 'docx' || $permi->extension_doc == 'xlx' || $permi->extension_doc == 'xlxs'){
                    $permi->type =  'office';
                }else{
                    $permi->type =  'image';
                }
                
                $new_array['extension_doc'] =  $permi->extension_doc; 
                $new_array['caption'] =  $permi->nombre.'.'.$permi->extension_doc;
                $new_array['key'] =  $permi->id;
                $new_array['type'] =  'office';
                $new_array['deleteUrl'] =  false;
                $new_array['downloadUrl'] =   url('img/planilla.docx');
                
                $extraplanillados2[] = $new_array;
                
                break;
            }
        }else{
            $extraplanillados = "[]";
            $extraplanillados2 = "[]";
        }
        
        if(!empty($orden->factura->id)){
            $extrafacturatodo = DocumentosExtra::where('id_documento',$orden->factura->id)->where('eliminado',0)->get();
            $extrafactura = [];  
            $extrafactura2 = [];  
            $i = 0;
            foreach($extrafacturatodo as $permi){
                $extrafactura[] = url('img/planilla.docx');
                $i++;
                if($permi->extension_doc == 'pdf'){
                    $permi->type =  'pdf';
                }else if($permi->extension_doc == 'doc' || $permi->extension_doc == 'docx' || $permi->extension_doc == 'xlx' || $permi->extension_doc == 'xlxs'){
                    $permi->type =  'office';
                }else{
                    $permi->type =  'image';
                }
                
                $new_array['extension_doc'] =  $permi->extension_doc; 
                $new_array['caption'] =  $permi->nombre.'.'.$permi->extension_doc;
                $new_array['key'] =  $permi->id;
                $new_array['type'] =  $permi->type;
                $new_array['deleteUrl'] =  false;
                $new_array['downloadUrl'] =   url('img/planilla.docx');
                
                $extrafactura2[] = $new_array;
                
                break;
            }
        }else{
            $extrafactura = "[]";
            $extrafactura2 = "[]";
        }

        if(!empty($orden->actualizacion->id)){
            $extraactualizaciontodo = DocumentosExtra::where('id_documento',$orden->actualizacion->id)->where('eliminado',0)->get();
            $extraactualizacion = [];  
            $extraactualizacion2 = [];  
            $i = 0;
            foreach($extraactualizaciontodo as $permi){
                $extraactualizacion[] = url('img/planilla.docx');
                $i++;
                if($permi->extension_doc == 'pdf'){
                    $permi->type =  'pdf';
                }else if($permi->extension_doc == 'doc' || $permi->extension_doc == 'docx' || $permi->extension_doc == 'xlx' || $permi->extension_doc == 'xlxs'){
                    $permi->type =  'office';
                }else{
                    $permi->type =  'image';
                }
                
                $new_array['extension_doc'] =  $permi->extension_doc; 
                $new_array['caption'] =  $permi->nombre.'.'.$permi->extension_doc;
                $new_array['key'] =  $permi->id;
                $new_array['type'] =  $permi->type;
                $new_array['deleteUrl'] =  false;
                $new_array['downloadUrl'] =   url('img/planilla.docx');
                
                $extraactualizacion2[] = $new_array;
                break;

            }
        }else{
            $extraactualizacion = "[]";
            $extraactualizacion2 = "[]";
        }

        if(!empty($orden->cambior->id)){
            $extracambiortodo = DocumentosExtra::where('id_documento',$orden->cambior->id)->where('eliminado',0)->get();
            $extracambior = [];  
            $extracambior2 = [];  
            $i = 0;
            foreach($extracambiortodo as $permi){
                $extracambior[] = url('img/planilla.docx');
                $i++;
                if($permi->extension_doc == 'pdf'){
                    $permi->type =  'pdf';
                }else if($permi->extension_doc == 'doc' || $permi->extension_doc == 'docx' || $permi->extension_doc == 'xlx' || $permi->extension_doc == 'xlxs'){
                    $permi->type =  'office';
                }else{
                    $permi->type =  'image';
                }
                
                $new_array['extension_doc'] =  $permi->extension_doc; 
                $new_array['caption'] =  $permi->nombre.'.'.$permi->extension_doc;
                $new_array['key'] =  $permi->id;
                $new_array['type'] =  $permi->type;
                $new_array['deleteUrl'] =  false;
                $new_array['downloadUrl'] =   url('img/planilla.docx');
                
                $extracambior2[] = $new_array;
                
                break;
            }
        }else{
            $extracambior = "[]";
            $extracambior2 = "[]";
        }

        $ext1 = json_encode($extrapermiso);
        $ext2 = json_encode($extrapermiso2);
        $extci1 = json_encode($extraci);
        $extci2 = json_encode($extraci2);
        $exttitulo1 = json_encode($extratitulo);
        $exttitulo2 = json_encode($extratitulo2);
        $extplanillauno1 = json_encode($extraplanillauno);
        $extplanillauno2 = json_encode($extraplanillauno2);
        $extplanillados1 = json_encode($extraplanillados);
        //return $extplanillados1;
        $extplanillados2 = json_encode($extraplanillados2);
        $extfacturauno1 = json_encode($extrafactura);
        $extfacturados2 = json_encode($extrafactura2);

        $extalizacionuno1 = json_encode($extraactualizacion);
        $extalizaciondos2 = json_encode($extraactualizacion2);
        
        $extacambior1 = json_encode($extracambior);
        $extacambior2 = json_encode($extracambior2);
        
        $fecha_vencimiento = !empty($orden->permiso->fecha_vencimiento) ? $orden->permiso->fecha_vencimiento : '00/00/0000';
       
        return view('cliente.detalle-orden',compact('id','extacambior1','extacambior2','orden','extfacturauno1','extfacturados2','extalizacionuno1','extalizaciondos2','ext1','ext2','extci1','extci2','exttitulo1','exttitulo2','id','extplanillauno1','extplanillauno2','extplanillados1','extplanillados2','fecha_vencimiento','datos'));
    }

    public function getObservaciones(){
        $cliente = Cliente::where('user_id',Auth::user()->id)->first();
        $observaciones = Observaciones::where('id_cliente',$cliente->id)->orderBy('fecha_registro')->get();
        foreach($observaciones as $observacion){
            $observacion->fecha_registro = Carbon::parse($observacion->fecha_registro)->format('d/m/Y H:i A');
        }
        return $observaciones;   
    }
    public function getBuscar($id){
        $orden = Ordenes::with('permiso' ,'ci' ,'titulo' ,'planillauno' ,'planillados','actualizacion','cambior' ,'cliente')->find($id);
        return $orden;   
    }


    public function getAprobaciondocumentos($id,Request $request){
        $orden = Ordenes::find($id); 
        
        $documento = Documento::find($request->documento_id);
        
        $band = 0;  
        if(!empty($documento)){

             if($request->aprobado != $documento->aprobado){
                $documento->aprobado = $request->aprobado;
                $documento->save();
             }
                   
        }
 
          
        $permiso = Documento::where('tipo_documento',1)->where('id_cliente',$orden->id_cliente)->where('aprobado',1)->where('habilitado',1)->first();
        $ci = Documento::where('tipo_documento',2)->where('id_cliente',$orden->id_cliente)->where('aprobado',1)->where('habilitado',1)->first();
        $titulo = Documento::where('tipo_documento',3)->where('id_cliente',$orden->id_cliente)->where('aprobado',1)->where('habilitado',1)->first();
        $planillaUno = Documento::where('tipo_documento',4)->where('id_cliente',$orden->id_cliente)->where('aprobado',1)->where('habilitado',1)->first();     
        $planillaDos = Documento::where('tipo_documento',5)->where('id_cliente',$orden->id_cliente)->where('aprobado',1)->where('habilitado',1)->first();
        //return $orden->id_cliente;
        if(!empty($permiso) && !empty($ci) && !empty($titulo) && !empty($planillaUno) && !empty($planillaDos)){
            //return 'dad';
        
             $orden->status = 'Aprobado';
             $orden->save();
             $role = Role::where('name','FACTURACION')->first();
             foreach($role->users as $user){
                 $emails[] = $user->email;
             }
  
             $titulo  = 'NUEVO PEDIDO PARA FACTURACIÓN';
             $mensaje = 'Te notificamos que hay una nueva solicitud pendiente para facturar de psicotropicos para cliente '.$orden->cliente->nombre_empresa;
             $mensaje2 = '';
             
             self::getEmailNotifica($emails,$titulo,$mensaje,$mensaje2);
             
             $titulo  = 'SU ORDEN FUE REVISADA Y SU SOLICITUD FUE APROBADA';
             $mensaje = 'Te notificamos que la solicitud de psicotropicos fue aprobada y fue transferia al area de facturación';
             $mensaje2 = '';
             self::getEmailNotifica([$orden->cliente->email_empresa,$orden->cliente->email_contacto],$titulo,$mensaje,$mensaje2);
             if(!empty($orden->cliente->vendedor)){
                 $titulo  = 'ORDEN FUE REVISADA Y APROBADA';
                 $mensaje = 'Te notificamos que la solicitud de psicotropicos de '.$orden->cliente->nombre_empresa.' fue aprobada y fue transferia al area de facturación';
                 $mensaje2 = '';
             }   
             if(!empty($orden->cliente->vendedor->email)){
 
             
                 self::getEmailNotifica([$orden->cliente->vendedor->email],$titulo,$mensaje,$mensaje2);
             }
         }else{
             $orden->status = 'Pendiente';
             $orden->save();
        
             
         }
         
         
         
         return 1;
    }

    public function postGuardarObservacion(Request $request){
      
        $orden = Ordenes::find($request->id_orden);
        $observacion = Observaciones::create([
            'observacion' => $request->observacion,
            'fecha_registro' => Carbon::now('America/Caracas'),
            'id_cliente' => $orden->cliente->id,
            'user_id' => Auth::user()->id,
            'orden_id' => $request->id_orden,
            'visto' => 0
        ]);
        
        $titulo  = 'SU ORDEN FUE REVISADA Y TIENE UNA OBSERVACIÓN';
        $mensaje = 'Te notificamos que la solicitud de psicotropicos esta pendiente y tienes la siguiente observación';
        $mensaje2 = $request->observacion;
        self::getEmailNotifica([$orden->cliente->email_empresa,$orden->cliente->email_contacto],$titulo,$mensaje,$mensaje2);
        if(!empty($orden->cliente->vendedor)){
            $titulo  = 'ORDEN CON OBSERVACIÓN';
            $mensaje = 'Te notificamos que la solicitud de psicotropicos de '.$orden->cliente->nombre_empresa.' esta pendiente y tiene la siguiente observación';
            $mensaje2 = $request->observacion;;
            self::getEmailNotifica([$orden->cliente->vendedor->email],$titulo,$mensaje,$mensaje2);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getEmailNotifica($emailscliente,$titulo,$mensaje,$mensaje2){
        
        $data = [
            'titulo' => $titulo,
            'mensaje' => $mensaje,
            'mensaje2' => $mensaje2
            ];
        
            $message = 'prueba';
            foreach($emailscliente as $email){
                if(!is_null($email)){
                    $emails[] = $email;
                }
            }
            $emails[] = 'marquez.steffani@gmail.com';
            $emails[] = 'agomez.servys@gmail.com';
            
            return  Mail::send('email.notificaciones', $data, function($message) use ($emails,$titulo)
            {
                
                $message->from('no-reply@linosgo.com', 'Droguería Servys');
                
                $message->to($emails)->subject($titulo);
                
            });

    }    
}
