<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentosExtra extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'documentos_extra';
    protected $fillable = ['url','nombre','extension_doc','tipo_documento','id_documento','eliminado'];
    
}
