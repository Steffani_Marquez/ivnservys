@extends('cliente.cliente-layout')
@section('title', 'Productos')
@section('enlace1', 'Home')
@section('enlace2', 'Productos')
@section('content')
<section class="py-5" ng-module="compraApp" ng-controller="compraController">
  <div class="container py-4">
    <div class="row g-5">
      <!-- SHOP SIDEBAR-->
      <div class="col-lg-3">
        <!-- Categories-->
        <h3 class="h4 lined text-uppercase mb-4">Categorias</h3>
        <ul class="nav flex-column nav-pills mb-4">
          <li class="nav-item"><a class="nav-link active" href="shop-category.html"> 
              <div class="d-flex align-items-center justify-content-between"><span class="fw-bold text-uppercase">Todos</span>
                <div class="badge bg-white text-primary">200</div>
              </div></a>
            <ul class="list-unstyled text-sm text-muted mb-0" hidden>
              <li><a class="nav-link ps-4 text-muted letter-spacing-1" href="shop-category.html"> <span class="ps-2">Ansiolíticos</span></a></li>
              <li><a class="nav-link ps-4 text-muted letter-spacing-1" href="shop-category.html"> <span class="ps-2">Hipolipemiantes</span></a></li>
              <li><a class="nav-link ps-4 text-muted letter-spacing-1" href="shop-category.html"> <span class="ps-2">Polivitamínicos</span></a></li>
              <li><a class="nav-link ps-4 text-muted letter-spacing-1" href="shop-category.html"> <span class="ps-2">Antibióticos</span></a></li>
            </ul>
          </li>
          <li class="nav-item"><a class="nav-link " href="shop-category.html" hidden> 
              <div class="d-flex align-items-center justify-content-between"><span class="fw-bold text-uppercase">Marcas</span>
                <div class="badge bg-secondary ">123</div>
              </div></a>
            <ul class="list-unstyled text-sm text-muted mb-0" hidden>
              <li><a class="nav-link ps-4 text-muted letter-spacing-1" href="shop-category.html"> <span class="ps-2">Laproff</span></a></li>
              <li><a class="nav-link ps-4 text-muted letter-spacing-1" href="shop-category.html"> <span class="ps-2">Biomedic</span></a></li>
              <li><a class="nav-link ps-4 text-muted letter-spacing-1" href="shop-category.html"> <span class="ps-2">La santé</span></a></li>
              <li><a class="nav-link ps-4 text-muted letter-spacing-1" href="shop-category.html"> <span class="ps-2">Caplin Point</span></a></li>
            </ul>
          </li>
          <li class="nav-item"><a class="nav-link" href="shop-category.html" > 
              <div class="d-flex align-items-center justify-content-between"><span class="fw-bold text-uppercase">Tipo</span>
                <div class="badge bg-secondary">11</div>
              </div></a>
            <ul class="list-unstyled text-sm text-muted mb-0" >
              <li><a class="nav-link ps-4 text-muted letter-spacing-1" href="shop-category.html"> <span class="ps-2">Analgésico</span></a></li>
              <li><a class="nav-link ps-4 text-muted letter-spacing-1" href="shop-category.html"> <span class="ps-2">Antigripales</span></a></li>
              <li><a class="nav-link ps-4 text-muted letter-spacing-1" href="shop-category.html"> <span class="ps-2">Psicotrópicos</span></a></li>
              <li><a class="nav-link ps-4 text-muted letter-spacing-1" href="shop-category.html"> <span class="ps-2">Antibióticos</span></a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- SHOP LISTING-->
      <div class="col-lg-9 mtxs">
        <p class="lead mb-2">Gran variedad de productos al mejor costo del mercado.</p>

        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Buscar</span>
          </div>
          <input type="text" class="form-control" ng-model="buscador">
        </div>
        
        <div class="row gy-5 align-items-stretch">

        <div class="col-lg-3 col-md-4 text-center" ng-repeat="producto in productos|filter:buscador">
                  <div class="product" >
                    <div class="product-image" ng-click="openmodal(producto)" style="cursor:pointer">
                      <img src="<% producto.url %>"  class="img-fluid data-url<% producto.id %>" >
                    </div>
                    <div class="">
                      <h4 class="mt-2 ">BS. 47.00</h4>
                      <h5 class="mt-2 texto nombre<% producto.id %>"><% producto.nombre %></h5>
                      <h5 class=" textodos mb-2" style="height: 20px"><span class="badge badge-warning">Fecha venc. <% producto.fv %></span></h5>
                      <h5 class="price"><% producto.precio %><h5>
                      <div class="contenbtns">
                        <span class="menus" ><img src="img/circulomenos.svg" class="masme" ng-click="restarppv_car(producto.id)"></span>
                        <span><input type="text" value="0" id="inputvalor<% producto.id %>" class="inputdel"></span> 
                        <span class="menus" ><img src="img/circulomas.svg" class="masme" ng-click="sumarppv_car(producto.id)"></span>
                      </div>
                      <input type="hidden" value="1" id="tipo<% producto.id %>" class="tipo">
                      <div class="rrow" ng-click="agregaralcarr(producto.id)">
                        <div  id="cajacarris<% producto.id %>">
                          <img src="img/carrito.svg" class="carrits"> Agregar al carrito
                        </div> 
                        <i class="fa fa-spinner fa-spin fa-3x fa-fw" id="load<% producto.id %>" style="font-size: 34px;display: none;" ></i>
                      </div>
                    </div>
                    <div class="carverde" style="display: none;" id="checkcar<% producto.id %>"><img src="img/carrito.svg" class="carrits"> Producto agregado al carrito
                    </div>
                  </div>
          </div>
              
          
        </div>

    </div>


    </div>
  </div>
  <div class="carfix">
    <a href="#" style="width: 100%;" id="carrito" ng-click="enviaCarrito()">
      <div class="redoncant" id="totalcarris">0</div>
      <img src="img/carrito.svg" class="carritsfix"> 
    </a>
  </div>
</section>

@include('cliente.modales.detail-product')
@endsection
@section('script')
<script src="{{url('js/angular_compra.js')}}"></script>
<script>

  
</script>
@endsection