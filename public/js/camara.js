var localstream, canvas, video, cxt;
    navigator.mediaDevices.enumerateDevices().then((devices) => {
        let videoSourcesSelect = document.getElementById("video-source");
    
        // Iterar sobre toda la lista de dispositivos (InputDeviceInfo y MediaDeviceInfo)
        devices.forEach((device) => {
            let option = new Option();
            option.value = device.deviceId;
    
            // Según el tipo de dispositivo multimedia
            switch(device.kind){
                // Agregar dispositivo a la lista de cámaras
                case "videoinput":
                    option.text = device.label || `Camera ${videoSourcesSelect.length + 1}`;
                    videoSourcesSelect.appendChild(option);
                    break;
                // Agregar dispositivo a la lista de micrófonos
            }
    
            console.log(device);
        });
    }).catch(function (e) {
        console.log(e.name + ": " + e.message);
    });


function turnOnCamera(a) {
    
    canvas = document.getElementById("canvas"+a);
    cxt = canvas.getContext("2d");
    video = document.getElementById("video"+a);
    $("#tipocarga"+a).val('camara');
    if(!navigator.getUserMedia)
        navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia || 
    navigator.msGetUserMedia;
    if(!window.URL)
        window.URL = window.webkitURL;

    if (navigator.getUserMedia) {
        navigator.getUserMedia({"video" : true, "audio": false
        }, function(stream) {
            console.log(stream);
            try {
                localstream = stream;
                video.srcObject = stream;
                video.play();
            } catch (error) {
                video.srcObject = null;
            }
        }, function(err){
            swal("Error", err, "error");
        });
    } else {
        swal("Mensaje", "User Media No Disponible" , "error");
        return;
    }
}
let videoSourcesSelect = document.getElementById("video-source");


 function cambiarcamara(a){
    MediaStreamHelper.requestStream().then(function(stream){
        MediaStreamHelper._stream = stream;
        videoPlayer.srcObject = stream;
        turnOnCamera(a);
    });
    
};

function turnOffCamera(a) {
    video.pause();
    video.srcObject = null;
    localstream.getTracks()[0].stop();
    $("#tipocarga"+a).val('archivo');
}

function tomarFoto(a){
    cxt.drawImage(video, 0, 0, 300, 150);
    var data = canvas.toDataURL("image/jpeg");
    var info = data.split(",", 2);

    $("#imagentomada"+a).attr('src',data);
    $("#fototomada"+a).val(info[1]);
    
    console.log(data);
}

$('#editarpermiso').click(function(){
    $('.notienepermiso').removeAttr('hidden');
    $('.tienepermiso').attr('hidden','hiiden');
});

$('#cancelar_edicion_permiso').click(function(){
    $('.tienepermiso').removeAttr('hidden');
    $('.notienepermiso').attr('hidden','hiiden');
});

$('#editarci').click(function(){
    $('.notieneci').removeAttr('hidden');
    $('.tieneci').attr('hidden','hiiden');
});

$('#cancelar_edicion_ci').click(function(){
    $('.tieneci').removeAttr('hidden');
    $('.notieneci').attr('hidden','hiiden');
});
$('#editartitulo').click(function(){
    $('.notienetitulo').removeAttr('hidden');
    $('.tienetitulo').attr('hidden','hiiden');
});

$('#cancelar_edicion_titulo').click(function(){
    $('.tienetitulo').removeAttr('hidden');
    $('.notienetitulo').attr('hidden','hiiden');
});