@extends('cliente.cliente-layout')
@section('title', 'Carrito')
@section('enlace1', 'Home')
@section('enlace2', 'Carrito')
@section('content')
<section class="py-5">  
    <div class="container py-4">
        <p class="text-muted lead mb-5">Tienes 3 items en tu carrito.</p>
        <div class="row">
        <div class="col-lg-9">
            <!-- CART PRODUCTS TABLE-->
            
            <div class="table-responsive">
                <table class="table text-nowrap">
                <thead>
                    <tr class="text-sm">
                    <th class="border-gray-300 border-top py-3" colspan="2">Producto</th>
                    <th class="border-gray-300 border-top py-3">Cantidad</th>
                    <th class="border-gray-300 border-top py-3">Precio unitario</th>
                    <th class="border-gray-300 border-top py-3">Total</th>
                    <th class="border-gray-300 border-top py-3"></th>
                    </tr>
                </thead>
                <tbody>
                <input type="hidden" id="datos" value="{{json_encode($datos)}}"> 
                    @foreach($datos as $dato)
                    <tr class="text-sm item{{$dato->id}}">
                        <td class="align-middle border-gray-300 py-3"><a href="#"><img class="img-fluid flex-shrink-0" src="{{$dato->url}}"  alt="White Blouse Armani" style="min-width: 50px" width="50"></a></td>
                        <td class="align-middle border-gray-300 py-3"><a href="#">{{$dato->nombre}}</a></td>
                        <td class="align-middle border-gray-300 py-3"> 
                            <input class="form-control" type="number" value="{{$dato->cantidad}}" style="max-width: 3.5rem" readonly>
                        </td>
                        <td class="align-middle border-gray-300 py-3">$10.00</td>
                        <td class="align-middle border-gray-300 py-3 stotal">{{$dato->cantidad * 10}}</td>
                        <td class="align-middle border-gray-300 py-3">
                            <button class="btn btn-link p-0 "  type="button" onclick="deleteItem({{$dato->id}})"><i class="fas fa-trash-alt"></i></button>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
                <tfoot>
                    <tr>
                    <th class="py-3 border-0" colspan="5"> <span class="h4 text-gray-700 mb-0">Total</span></th>
                    <th class="py-3 border-0 text-end" colspan="2"> <span class="h4 text-gray-700 mb-0 totalgenerral">$446.00</span></th>
                    </tr>
                </tfoot>
                </table>
            </div>
            <!-- NAVIGATION FOOTER-->
            <div class="row gx-lg-0 align-items-center bg-light px-4 py-3 text-center mb-5">
                <div class="col-md-6 text-md-start py-1"><a class="btn btn-secondary my-1" href="/compra"><i class="fas fa-angle-left me-1"></i> Continua Comprando</a></div>
                <div class="col-md-6 text-md-end py-1">
                <button class="btn btn-secondary my-1" hidden><i class="fas fa-sync-alt me-1"></i> Update cart</button>
                <a class="btn btn-outline-primary my-1" href="img/planilla.docx" onclick="finish()">Siguinte <i class="fas fa-angle-right ms-1"></i></a>
                </div>
            </div>
            <!-- RELATED PRODUCTS-->
            
        </div>
        <!-- CHECKOUT SIDEBAR [ORDER SUMMARY]-->
        <div class="col-lg-3">
            <div class="mb-5">
            <div class="p-4 bg-gray-200">
                <h3 class="text-uppercase mb-0">Resumen de compra</h3>
            </div>
            <div class="bg-light py-4 px-3">
                <p class="text-muted" hidden></p>
                <div class="table-responsive">
                <table class="table mb-0">
                    <tbody class="text-sm">
                    <tr>
                        <th class="text-muted"> <span class="d-block py-1 fw-normal">Subtotal</span></th>
                        <th> <span class="d-block py-1 fw-normal text-end totalgenerral">$446.00</span></th>
                    </tr>
                    <tr>
                        <th class="text-muted"> <span class="d-block py-1 fw-normal">Descuento</span></th>
                        <th> <span class="d-block py-1 fw-normal text-end">0</span></th>
                    </tr>
                    <tr>
                        <th class="text-muted"> <span class="d-block py-1 fw-normal">Días Credito</span></th>
                        <th> <span class="d-block py-1 fw-normal text-end">3</span></th>
                    </tr>
                    <tr class="total">
                        <td class="py-3 border-bottom-0 text-muted"> <span class="lead fw-bold">Total</span></td>
                        <th class="py-3 border-bottom-0"> <span class="lead fw-bold text-end totalgenerral">$456.00</span></th>
                    </tr>
                    </tbody>
                </table>
                </div>
            </div>
            </div>
 
        </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
function deleteItem(id){
    $(`.item${id}`).remove();
    total()
}

function total(){
    var totalgenerral = 0;
    $(".stotal").each(function(){
        totalgenerral += Number($(this).text())
    });
    $(".totalgenerral").text(`$${totalgenerral}`);
}
total()

function finish(){
   
    Swal.fire({
        title: 'Orden creada',
        text: 'Tu orden ha sido creada exitosamente',
        icon: 'success',
        confirmButtonText: 'ok'
    }).then((result) => {
        
        datos = $('#datos').val();
        setTimeout(function() {
            window.location =  'orders-02?datos='+datos;
        },1000);
    })
}
</script>
@endsection