<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Observaciones extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'observaciones';
    protected $fillable = ['observacion','fecha_registro','id_cliente','user_id','orden_id','visto'];

 

}
