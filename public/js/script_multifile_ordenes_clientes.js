

$(document).on('click', '.kv-file-zoom', function () {
    
    $('#kvFileinputModal').removeClass('fade');
    
});



$(document).ready(function() {
    
    $('#tipo_documento').change(function(){
        if($(this).val() == 1){
            $('.fecha_vencimiento').removeAttr('hidden');
        }else{
            $('.fecha_vencimiento').attr('hidden','hidden');
        }
        tipo_documento = $('#tipo_documento').val(); 
    });

    if($('#extradocpermiso').val() == '[]'){
        datos = false;
        datosarray = false;
    }else{
        datos = JSON.parse($('#extradocpermiso').val());
        datosarray = JSON.parse($('#extradocpermiso2').val());
    }
    
    if($('#extradocci1').val() == '[]'){
        datos2 = false;
        datosarray2 = false;
    }else{
        datos2 = JSON.parse($('#extradocci1').val());
        datosarray2 = JSON.parse($('#extradocci2').val());
    }

    if($('#extradoctitulo1').val() == '[]'){
        datos3 = false;
        datosarray3 = false;
    }else{
        datos3 = JSON.parse($('#extradoctitulo1').val());
        datosarray3 = JSON.parse($('#extradoctitulo2').val());
    }
    
    if($('#extplanillauno1').val() == '[]'){
        datos4 = false;
        datosarray4 = false;
    }else{
        datos4 = JSON.parse($('#extplanillauno1').val());
        datosarray4 = JSON.parse($('#extplanillauno2').val());
    }
    
    if($('#extplanillados1').val() == '[]'){
        datos5 = false;
        datosarray5 = false;
    }else{
        datos5 = JSON.parse($('#extplanillados1').val());
        datosarray5 = JSON.parse($('#extplanillados2').val());
    }

    if($('#extfactura1').val() == '[]'){
        datos6 = false;
        datosarray6 = false;
    }else{
        datos6 = JSON.parse($('#extfactura1').val());
        datosarray6 = JSON.parse($('#extfactura2').val());
    }

    if($('#extactualizacion1').val() == '[]'){
        datos7 = false;
        datosarray7 = false;
    }else{
        datos7 = JSON.parse($('#extactualizacion1').val());
        datosarray7 = JSON.parse($('#extactualizacion2').val());
    }    
    
    if($('#extcambior1').val() == '[]'){
        datos8 = false;
        datosarray8 = false;
    }else{
        datos8 = JSON.parse($('#extcambior1').val());
        datosarray8 = JSON.parse($('#extcambior2').val());
    }

    if($('#fecha_vencimiento').val() == ''){
         fechaVencimiento = '';
    }else{
         fechaVencimiento = $('#fecha_vencimiento').val();

    }
                     
   
    $("#archivo1").fileinput({
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos == "[]") ? false : datos,
        //initialPreview: [
        //    'https://linosgo.com/servyspsicotropicos/uploads/4085223.pdf'
        //],
        //initialPreviewConfig: [
        //    {type: 'pdf', description: "<h5>PDF File One</h5> This is a representative placeholder description number one for this PDF file.", size: 2050}
        //],
        initialPreviewConfig:  (datos == "[]") ? false : datosarray, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:false,
        showRemove:false,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image' || config.type === 'office') {
                    return true;
                }
                return false;
            }
        }
    });

    $("#archivo2").fileinput({
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos2 == "[]") ? false : datos2,
        //initialPreview: [
        //    'https://linosgo.com/servyspsicotropicos/uploads/4085223.pdf'
        //],
        //initialPreviewConfig: [
        //    {type: 'pdf', description: "<h5>PDF File One</h5> This is a representative placeholder description number one for this PDF file.", size: 2050}
        //],
        initialPreviewConfig:  (datos2 == "[]") ? false : datosarray2, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:false,
        showRemove:false,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image' || config.type === 'office') {
                    return true;
                }
                return false;
            }
        }
    });
     
    $("#archivo3").fileinput({
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos3 == "[]") ? false : datos3,
        //initialPreview: [
        //    'https://linosgo.com/servyspsicotropicos/uploads/4085223.pdf'
        //],
        //initialPreviewConfig: [
        //    {type: 'pdf', description: "<h5>PDF File One</h5> This is a representative placeholder description number one for this PDF file.", size: 2050}
        //],
        initialPreviewConfig:  (datos3 == "[]") ? false : datosarray3, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:false,
        showRemove:false,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image' || config.type === 'office') {
                    return true;
                }
                return false;
            }
        }
    });  
      
    $("#archivo4").fileinput({
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos4 == "[]") ? false : datos4,
        //initialPreview: [
        //    'https://linosgo.com/servyspsicotropicos/uploads/4085223.pdf'
        //],
        //initialPreviewConfig: [
        //    {type: 'pdf', description: "<h5>PDF File One</h5> This is a representative placeholder description number one for this PDF file.", size: 2050}
        //],
        initialPreviewConfig:  (datos4 == "[]") ? false : datosarray4, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:false,
        showRemove:false,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image' || config.type === 'office') {
                    return true;
                }
                return false;
            }
        }
    });  
      
    $("#archivo5").fileinput({
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos5 == "[]") ? false : datos5,
        //initialPreview: [
        //    'https://linosgo.com/servyspsicotropicos/uploads/4085223.pdf'
        //],
        //initialPreviewConfig: [
        //    {type: 'pdf', description: "<h5>PDF File One</h5> This is a representative placeholder description number one for this PDF file.", size: 2050}
        //],
        initialPreviewConfig:  (datos5 == "[]") ? false : datosarray5, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:false,
        showRemove:false,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image' || config.type === 'office') {
                    return true;
                }
                return false;
            }
        }
    });       

    $("#archivo6").fileinput({
        uploadUrl: "profile-02/upload-file",
        enableResumableUpload: false,
        uploadExtraData: {
            'tipo_documento': 6, // for access control / security 
        },
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos6 == "[]") ? false : datos6,
        initialPreviewConfig:  (datos6 == "[]") ? false : datosarray6, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:true,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            }
        }
        /*
        layoutTemplates: {
            actions: `
            <button type="button" class="kv-file-check btn btn-sm btn-kv btn-default btn-outline-secondary" title="check"><i class="fas fa-check"></i></button>
            `    
        }
        */
    }); 
    
    $("#archivo7").fileinput({
        uploadUrl: "profile-02/upload-file",
        enableResumableUpload: false,
        uploadExtraData: {
            'tipo_documento': 7, // for access control / security 
        },
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos7 == "[]") ? false : datos7,
        initialPreviewConfig:  (datos7 == "[]") ? false : datosarray7, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:true,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            }
        }
        /*
        layoutTemplates: {
            actions: `
            <button type="button" class="kv-file-check btn btn-sm btn-kv btn-default btn-outline-secondary" title="check"><i class="fas fa-check"></i></button>
            `    
        }
        */
    }); 

    $("#archivo8").fileinput({
        uploadUrl: "profile-02/upload-file",
        enableResumableUpload: false,
        uploadExtraData: {
            'tipo_documento': 8, // for access control / security 
        },
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos8 == "[]") ? false : datos8,
        initialPreviewConfig:  (datos8 == "[]") ? false : datosarray8, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:true,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            }
        }
        /*
        layoutTemplates: {
            actions: `
            <button type="button" class="kv-file-check btn btn-sm btn-kv btn-default btn-outline-secondary" title="check"><i class="fas fa-check"></i></button>
            `    
        }
        */
    }); 
});
