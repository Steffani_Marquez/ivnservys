	
		var AllPost_app = angular.module('ProductosApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('ProductosController', function($scope, $http) {
    
            
            $scope.cambiar_status = function(id) { 
                //alert('sdfsf');
                $('.modal_status').modal('toggle');
                $('#producto_id').val(id);
            }
            //$scope.editando_status(){

            //}

            $scope.cambiar_producto = function(id) { 
                $('#imgSalida').attr('src','');
                $('.modal_editar_producto').modal('toggle'); 
                $('#producto_id').val(id);
                
                $http.get('/producto/producto',{params: {id_producto:id }}).
				success(function(data, status, headers, config){
                    $scope.producto = data;    
                    $('#imgSalida').attr('src',data.image);
				});
                
            }            
            
            /*
            $scope.editando_producto = function(){
                var formData = new FormData();
                var files = $('#file-image')[0].files[0];
                $http.post('/producto/guardar-producto',{params: {id_producto:$('#producto_id').val(), code: $('#code').val(), cod_barra: $('#cod_barra').val(), short_name:$('#short_name').val(), description:$('#description').val(), fecha_vencimiento:$('#fecha_vencimiento').val(), stock:$('#stock').val(), precio_maximo:$('#precio_maximo').val(), estatus_producto:$('#estatus_producto').val(),image: files },headers:{'Content-Type': 'multipart/form-data'}}).
				success(function(data, status, headers, config){
                    $scope.producto = data;
                    $('#imgSalida').attr('src',data.image);
                    
                    if($scope.producto.status == 'ACTIVO'){
                        $('.status_table'+$('#producto_id').val()).css('background-color','#28a745');
                    }else{
                        $('.status_table'+$('#producto_id').val()).css('background-color','#dc3545');
                    }
                    $('.status_table'+$('#producto_id').val()).text($scope.producto.status);
                    
				});
            }
            */
            $scope.editando_status = function(){
                $http.get('/producto/changestatus',{params: {id_producto:$('#producto_id').val(), status: $('#status').val() }}).
				success(function(data, status, headers, config){
                    $scope.estatus_obj = data;
                    //console.log($scope.estatus_obj);
                    //console.log($scope.color_rgb);
                    if($scope.estatus_obj.status == 'ACTIVO'){
                        $('.status_table'+$('#producto_id').val()).css('background-color','#28a745');
                    }else{
                        $('.status_table'+$('#producto_id').val()).css('background-color','#dc3545');
                    }
                    $('.status_table'+$('#producto_id').val()).text($scope.estatus_obj.status);
                    $('.modal_status').modal('hide');
                    
				});
            }

		});