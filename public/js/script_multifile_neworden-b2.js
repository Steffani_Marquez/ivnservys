

$(document).ready(function() {
    if($('#extraplanillaUno').val() == '[]'){
        datos = false;
        datosarray = false;
    }else{
        datos = JSON.parse($('#extraplanillaUno').val());
        datosarray = JSON.parse($('#extraplanillaUno2').val());
    }
    /*
    $('#tipo_documento').change(function(){
        if($(this).val() == 1){
            $('.fecha_vencimiento').removeAttr('hidden');
        }else{
            $('.fecha_vencimiento').attr('hidden','hidden');
        }
        tipo_documento = $('#tipo_documento').val(); 
    });

    


    if($('#extradocci1').val() == '[]'){
        datos2 = false;
        datosarray2 = false;
    }else{
        datos2 = JSON.parse($('#extradocci1').val());
        datosarray2 = JSON.parse($('#extradocci2').val());
    }

    if($('#extradoctitulo1').val() == '[]'){
        datos3 = false;
        datosarray3 = false;
    }else{
        datos3 = JSON.parse($('#extradoctitulo1').val());
        datosarray3 = JSON.parse($('#extradoctitulo2').val());
    }

    if($('#fecha_vencimiento').val() == ''){
         fechaVencimiento = '';
    }else{
         fechaVencimiento = $('#fecha_vencimiento').val();

    }
    */                 
   var count = 0;
    $("#archivo1").fileinput({
        uploadUrl: "upload-file",
        enableResumableUpload: false,
        uploadExtraData: {
            'tipo_documento': 4, // for access control / security 
        },
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos == "[]") ? false : datos,
        //initialPreview: [
        //    'https://linosgo.com/servyspsicotropicos/uploads/4085223.pdf'
        //],
        //initialPreviewConfig: [
        //    {type: 'pdf', description: "<h5>PDF File One</h5> This is a representative placeholder description number one for this PDF file.", size: 2050}
        //],
        initialPreviewConfig:  (datos == "[]") ? false : datosarray, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:true,
        showBrowse: true,
        browseOnZoneClick: true,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image' || config.type === 'office') {
                    return true;
                }
                return false;
            }
        }
    }).on('filebeforedelete', function(event, key, data) {
        $.ajax({
            method: "POST",
            url: "has-orden",
            data: { key: key }
          }).done(function( msg ) {
              if(msg != 0){
                alert(  msg );
              }
              window.location = 'https://linosgo.com/servyspsicotropicos/orders-02/create';
            });
        
    }).on('fileuploaded', function(event, data, previewId, index, fileId) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
            console.log(response);

            if(response.error == ''){
                $('.alert-error').removeAttr('hidden');
                $('#error').text(response.error);
            }else{
                let filesCount = $("#archivo1").fileinput('getFilesCount');
     
   
                if(filesCount == 1){                
                    alert( 'Relacion cargada exitosamente' );
                    window.location = 'https://linosgo.com/servyspsicotropicos/orders-02/create';
                }
        
            }
     
            
            

    });

        $("#archivo2").fileinput({
        uploadUrl: "upload-file",
        enableResumableUpload: false,
        uploadExtraData: {
            'tipo_documento': 5, // for access control / security 
        },
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:true,
        showBrowse: true,
        browseOnZoneClick: true,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            }
        }
        /*
        layoutTemplates: {
            actions: `
            <button type="button" class="kv-file-check btn btn-sm btn-kv btn-default btn-outline-secondary" title="check"><i class="fas fa-check"></i></button>
            `    
        }
        */
    }).on('filebeforedelete', function(event, key, data) {
        $.ajax({
            method: "POST",
            url: "has-orden",
            data: { key: key }
          }).done(function( msg ) {
            if(msg != 0){
                alert(  msg );
              }
              //window.location = 'orders-02';
            });
        
    }).on('filepreupload', function(event, data, previewId, index, fileId) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
            //count = data.filescount;
            //console.log('count pre', count);
            

    }).on('fileuploaded', function(event, data, previewId, index, fileId) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
            let filesCount = $("#archivo2").fileinput('getFilesCount');
     
            console.log(filesCount);
        
            if(filesCount == 1){
                if(data.response.initialPreviewConfig[0].tipo_documento == 5){
                    
                
                    $.ajax({
                        method: "POST",
                        url: "save-orden",
                        data: { tipo_documento: data.response.initialPreviewConfig[0].tipo_documento, id_documento : data.response.initialPreviewConfig[0].id_documento, fecha_pedido: $('#fecha_pedido').val() }
                    }).done(function( msg ) {
                        alert(  msg );
                        console.log(msg );
                        window.location = 'https://linosgo.com/servyspsicotropicos/orders-02';
                    });
                }    
            }
            //console.log('count post', count);
            //console.log('count post', data.filescount);
            

    });
     
});
