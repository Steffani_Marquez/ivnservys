		
		var AllPost_app = angular.module('ordenesApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('ordenesController', function($scope, $http) {

 
			$scope.ordenes_arr = [];
			$http.get('control-facturacion-02/buscar', {params: { status:'Aprobado', desde: $('#desde').val() , hasta: $('#hasta').val()}}).
			success(function(data, status, headers, config){
			$scope.ordenes_arr = data;
			
			});

			$scope.buscar = function(status){

				$scope.ordenes_arr = [];
				$http.get('control-facturacion-02/buscar', {params: { status: status, desde: $('#desde').val() , hasta: $('#hasta').val() }}).
				success(function(data, status, headers, config){
				$scope.ordenes_arr = data;
				});
			}
			
			$scope.openmodaldetalle = function(orden){
				$('#openmodaldetalle').modal('toggle');
				$scope.orden = orden;
			}					

			$scope.openmodalstatus = function(id){
				$('#openmodalstatus').modal('toggle');
				$('#orden_id').val(id);
			}					

			$scope.cambiarstatus = function(){

				$scope.ordenes_arr = [];
				$http.get('control-facturacion-02/cambiarstatus', {params: { id: $('#orden_id').val(), status: $('#estatus').val() }}).
				success(function(data, status, headers, config){
					$scope.ordenes_arr = data;
					$scope.buscar();
				});
                $('#openmodalstatus').modal('toggle');

			}
                        
            

	});