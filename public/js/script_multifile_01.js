$("#archivo1").change(function () {
    filePreview1(this);

    alert("hola");
});


function filePreview1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#uploadForm1 + img').remove();
            $('#uploadForm1').after('<img src="'+e.target.result+'" />');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(document).on('click', '.kv-file-zoom', function () {
    
    $('#kvFileinputModal').removeClass('fade');
    
});



$(document).ready(function() {

    $('#tipo_documento').change(function(){
        if($(this).val() == 1){
            $('.fecha_vencimiento').removeAttr('hidden');
        }else{
            $('.fecha_vencimiento').attr('hidden','hidden');
        }
        tipo_documento = $('#tipo_documento').val(); 
    });

    if($('#extradocpermiso').val() == '[]'){
        datos = false;
        datosarray = false;
    }else{
        datos = JSON.parse($('#extradocpermiso').val());
        datosarray = JSON.parse($('#extradocpermiso2').val());
    }

    if($('#extradocci1').val() == '[]'){
        datos2 = false;
        datosarray2 = false;
    }else{
        datos2 = JSON.parse($('#extradocci1').val());
        datosarray2 = JSON.parse($('#extradocci2').val());
    }

    if($('#extradoctitulo1').val() == '[]'){
        datos3 = false;
        datosarray3 = false;
    }else{
        datos3 = JSON.parse($('#extradoctitulo1').val());
        datosarray3 = JSON.parse($('#extradoctitulo2').val());
    }


    if($('#extradocalizacion1').val() == '[]'){
        datos4 = false;
        datosarray4 = false;
    }else{
        datos4 = JSON.parse($('#extradocalizacion1').val());
        datosarray4 = JSON.parse($('#extradocalizacion2').val());
    }

    if($('#extradoccambior1').val() == '[]'){
        datos5 = false;
        datosarray5 = false;
    }else{
        datos5 = JSON.parse($('#extradoccambior1').val());
        datosarray5 = JSON.parse($('#extradoccambior2').val());
    }

    $('#fecha_vencimiento').on('change',function(){
        alert('change')
        fechaVencimiento = $('#fecha_vencimiento').val();
    });
                     
    console.log(datos);
    $("#archivo1").fileinput({
        uploadUrl: "profile-02/upload-file",
        enableResumableUpload: false,
        uploadExtraData: {
            'tipo_documento': 1, // for access control / security 
        },
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos == "[]") ? false : datos,
        //initialPreview: [
        //    'https://linosgo.com/servyspsicotropicos/uploads/4085223.pdf'
        //],
        //initialPreviewConfig: [
        //    {type: 'pdf', description: "<h5>PDF File One</h5> This is a representative placeholder description number one for this PDF file.", size: 2050}
        //],
        initialPreviewConfig:  (datos == "[]") ? false : datosarray, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:true,
        showBrowse: true,
        browseOnZoneClick: true,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image' || config.type === 'office') {
                    return true;
                }
                return false;
            }
        }
    }).on('filebeforedelete', function(event, key, data) {
        $.ajax({
            method: "POST",
            url: "profile-02/has-orden",
            data: { key: key }
          }).done(function( msg ) {
            if(msg != 0){
                alert(  msg );
            }
            window.location = 'profile-02';
            });
        
    }).on('filepreajax', function(event, previewId, index) {
        
        
        previewId.append("fecha_vencimiento", $('#fecha_vencimiento').val());
        console.log(previewId);
       
    });

        $("#archivo2").fileinput({
        uploadUrl: "profile-02/upload-file",
        enableResumableUpload: false,
        uploadExtraData: {
            'tipo_documento': 2, // for access control / security 
        },
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos2 == "[]") ? false : datos2,
        initialPreviewConfig:  (datos2 == "[]") ? false : datosarray2, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:true,
        showBrowse: true,
        browseOnZoneClick: true,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            }
        }
        /*
        layoutTemplates: {
            actions: `
            <button type="button" class="kv-file-check btn btn-sm btn-kv btn-default btn-outline-secondary" title="check"><i class="fas fa-check"></i></button>
            `    
        }
        */
    }).on('filebeforedelete', function(event, key, data) {
        $.ajax({
            method: "POST",
            url: "profile-02/has-orden",
            data: { key: key }
          }).done(function( msg ) {
            if(msg != 0){
                alert(  msg );
            }
            window.location = 'profile-02';
            });
        
    });

    $("#archivo3").fileinput({
        uploadUrl: "profile-02/upload-file",
        enableResumableUpload: false,
        uploadExtraData: {
            'tipo_documento': 3, // for access control / security 
        },
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos3 == "[]") ? false : datos3,
        initialPreviewConfig:  (datos3 == "[]") ? false : datosarray3, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:true,
        showBrowse: true,
        browseOnZoneClick: true,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            }
        }
        /*
        layoutTemplates: {
            actions: `
            <button type="button" class="kv-file-check btn btn-sm btn-kv btn-default btn-outline-secondary" title="check"><i class="fas fa-check"></i></button>
            `    
        }
        */
    }).on('filebeforedelete', function(event, key, data) {
        $.ajax({
            method: "POST",
            url: "profile-02/has-orden",
            data: { key: key }
          }).done(function( msg ) {
            if(msg != 0){
                alert(  msg );
            }
            window.location = 'profile-02';
            });
        
    });
    

    $("#archivo4").fileinput({
        uploadUrl: "profile-02/upload-file",
        enableResumableUpload: false,
        uploadExtraData: {
            'tipo_documento': 7, // for access control / security 
        },
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos4 == "[]") ? false : datos4,
        initialPreviewConfig:  (datos4 == "[]") ? false : datosarray4, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:true,
        showBrowse: true,
        browseOnZoneClick: true,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            }
        }
        /*
        layoutTemplates: {
            actions: `
            <button type="button" class="kv-file-check btn btn-sm btn-kv btn-default btn-outline-secondary" title="check"><i class="fas fa-check"></i></button>
            `    
        }
        */
    }).on('filebeforedelete', function(event, key, data) {
        $.ajax({
            method: "POST",
            url: "profile-02/has-orden",
            data: { key: key }
          }).done(function( msg ) {
            if(msg != 0){
                alert(  msg );
            }
            window.location = 'profile-02';
            });
        
    });

    $("#archivo5").fileinput({
        uploadUrl: "profile-02/upload-file",
        enableResumableUpload: false,
        uploadExtraData: {
            'tipo_documento': 8, // for access control / security 
        },
        usePdfRenderer: true,
        pdfRendererUrl: 'https://linosgo.com/servyspsicotropicos/pdfview',
        initialPreviewAsData: true,
        initialPreview: (datos5 == "[]") ? false : datos5,
        initialPreviewConfig:  (datos5 == "[]") ? false : datosarray5, 
        overwriteInitial: false,
        maxImageWidth: 1600,
        maxImageHeight: 1600,
        maxFileSize: 10000,
        resizePreference: 'height',
        resizeImage: true,
        resizeIfSizeMoreThan: 1000,
        theme: 'fas',
        showZoom: true,
        showUpload:true,
        showBrowse: true,
        browseOnZoneClick: true,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            }
        }
        /*
        layoutTemplates: {
            actions: `
            <button type="button" class="kv-file-check btn btn-sm btn-kv btn-default btn-outline-secondary" title="check"><i class="fas fa-check"></i></button>
            `    
        }
        */
    }).on('filebeforedelete', function(event, key, data) {
        $.ajax({
            method: "POST",
            url: "profile-02/has-orden",
            data: { key: key }
          }).done(function( msg ) {
            if(msg != 0){
                alert(  msg );
            }
            window.location = 'profile-02';
            });
        
    });
    
          
});
