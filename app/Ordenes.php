<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordenes extends Model
{
    //
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'ordenes';
    protected $fillable = ['id_permiso','id_ciregente','id_titulo','id_planilla1','id_planilla2','id_factura','id_actualizacion','id_cambior','fecha_pedido','fecha_creado','status','id_cliente','observacion'];
    
    public function permiso() {
        return $this->hasOne('App\Documento','id','id_permiso');
    }

    public function ci() {
        return $this->hasOne('App\Documento','id','id_ciregente');
    }

    public function titulo() {
        return $this->hasOne('App\Documento','id','id_titulo');
    }        
    
    public function planillauno() {
        return $this->hasOne('App\Documento','id','id_planilla1');
    }        
    
    public function planillados() {
        return $this->hasOne('App\Documento','id','id_planilla2');
    }

    public function factura() {
        return $this->hasOne('App\Documento','id','id_factura');
    } 

    public function actualizacion() {
        return $this->hasOne('App\Documento','id','id_actualizacion');
    } 
    public function cambior() {
        return $this->hasOne('App\Documento','id','id_cambior');
    }     
    public function cliente() {
        return $this->hasOne('App\Cliente','id','id_cliente');
    }            
}
