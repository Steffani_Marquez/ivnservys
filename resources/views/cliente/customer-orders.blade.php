@extends('cliente.cliente-layout')
@section('title', 'Ordenes')
@section('enlace1', 'Home')
@section('enlace2', 'Ordenes')
@section('content')
<section class="py-5">
    <div class="container py-4">
        <div class="row gy-5">
        <div class="col-lg-12">
            <p class="text-muted lead mb-5">Si tiene una inquietud contacte a su asesor de venta</p>
            <!-- ORDERS TABLE-->
            <div class="table-responsive">
            <table class="table table-hover text-nowrap">
                <thead>
                <tr class="text-sm">
                    <th class="border-gray-300 border-top py-3">Orden</th>
                    <th class="border-gray-300 border-top py-3">Fecha</th>
                    <th class="border-gray-300 border-top py-3">Total</th>
                    <th class="border-gray-300 border-top py-3">Estatus</th>
                    <th class="border-gray-300 border-top py-3">Acción</th>
                </tr>
                </thead>
                <tbody>
                <tr class="text-sm">
                    <th class="align-middle py-3"># 1735</th>
                    <td class="align-middle py-3">22/06/2013</td>
                    <td class="align-middle py-3">$ 150.00</td>
                    <td class="align-middle py-3"><span class="badge fw-light text-uppercase bg-info">Pendiente</span></td>
                    <td class="align-middle py-3"><a class="btn btn-outline-primary btn-sm" href="detalle-orden?datos={{json_encode($datos)}}">Ver</a></td>
                </tr>
                <tr class="text-sm" hidden>
                    <th class="align-middle py-3"># 1735</th>
                    <td class="align-middle py-3">22/06/2013</td>
                    <td class="align-middle py-3">$ 150.00</td>
                    <td class="align-middle py-3"><span class="badge fw-light text-uppercase bg-info">Being prepared</span></td>
                    <td class="align-middle py-3"><a class="btn btn-outline-primary btn-sm" href="customer-order.html">View</a></td>
                </tr>
                <tr class="text-sm" hidden>
                    <th class="align-middle py-3"># 1735</th>
                    <td class="align-middle py-3">22/06/2013</td>
                    <td class="align-middle py-3">$ 150.00</td>
                    <td class="align-middle py-3"><span class="badge fw-light text-uppercase bg-primary">Received</span></td>
                    <td class="align-middle py-3"><a class="btn btn-outline-primary btn-sm" href="customer-order.html">View</a></td>
                </tr>
                <tr class="text-sm" hidden>
                    <th class="align-middle py-3"># 1735</th>
                    <td class="align-middle py-3">22/06/2013</td>
                    <td class="align-middle py-3">$ 150.00</td>
                    <td class="align-middle py-3"><span class="badge fw-light text-uppercase bg-danger">Cancelled</span></td>
                    <td class="align-middle py-3"><a class="btn btn-outline-primary btn-sm" href="customer-order.html">View</a></td>
                </tr>
                <tr class="text-sm" hidden>
                    <th class="align-middle py-3"># 1735</th>
                    <td class="align-middle py-3">22/06/2013</td>
                    <td class="align-middle py-3">$ 150.00</td>
                    <td class="align-middle py-3"><span class="badge fw-light text-uppercase bg-warning">On hold</span></td>
                    <td class="align-middle py-3"><a class="btn btn-outline-primary btn-sm" href="customer-order.html">View</a></td>
                </tr>
                </tbody>
            </table>
            </div>
        </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
function deleteItem(id){
    $(`.item${id}`).remove();
    total()
}

function total(){
    var totalgenerral = 0;
    $(".stotal").each(function(){
        totalgenerral += Number($(this).text())
    });
    $(".totalgenerral").text(`$${totalgenerral}`);
}
total()

function finish(){

    Swal.fire({
        title: 'Orden creada',
        text: 'Tu orden ha sido creada exitosamente',
        icon: 'success',
        confirmButtonText: 'ok'
    }).then((result) => {
        
        datos = $('#datos').val();
        setTimeout(function() {
            window.location = 'orders-02?datos='+datos;
        },1000);
    })
}
</script>
@endsection