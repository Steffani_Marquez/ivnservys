	
		var AllPost_app = angular.module('AdsalvavidasApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('SalvavidasController', function($scope, $http) {

	
			
	        	$scope.cambiotipodeli = function(id) { 

	        		
                     $(".botondeli").removeClass( "activedeli" );
	        		if (id == 1) {

	        			$("#paga_ahora").addClass( "activedeli" );
	        			$(".contpago").show();
						$(".contsaltarpago").hide();

	        		}else{
	        			$("#saltar_pago").addClass( "activedeli" );
	        			$(".contsaltarpago").show();
						$(".contpago").hide();
	        		}

	        	}
			
				
				$scope.nueva_direccion = function(){
					
					if($('.add-direction').hasClass('oculta')){
						$('.add-direction').removeAttr('hidden');
						$('.add-direction').removeClass('oculta');
						$('.select-direccion').attr('hidden','hidden'); 
						$('#direccion').attr('required','required');
						$('#zona').attr('required','required');
						$('#direccion_id').removeAttr('required');
						$('#direccion_id').val('');
					}else{
						$('.add-direction').attr('hidden','hidden');
						$('.add-direction').addClass('oculta');
						$('#direccion_id').attr('required','required');
						$('#direccion').removeAttr('required');
						$('#zona').removeAttr('required');
						$('.select-direccion').removeAttr('hidden');
					}
				}

				$scope.selectmediopago = function(){
					$('.medio_pago_class').addClass('SelectedMedio');
				}
			
               	$scope.carritoprod = [];
				$scope.totalbss = 0;
				$http.get('/buscarcarritocheck').
				success(function(data, status, headers, config){
				$scope.carritoprod = data;
				//console.log($scope.carritoprod[0].tipo);
				$('#cargador').attr('hidden','hidden');
				$('#btncontinuar').removeAttr('disabled');
				$('#btnsiguiente').removeAttr('disabled');
				
				});

				$scope.productos_arr = [];
				$http.get('/buscarproductos').
				success(function(data, status, headers, config){
				$scope.productos_arr = data;
				$('#cargador').attr('hidden','hidden');
				$('#btncontinuar').removeAttr('disabled');
				$('#btnsiguiente').removeAttr('disabled');
				
				});



				$http.get('/buscarcarrito').
				success(function(data, status, headers, config){
				   $("#totalcarris").text(data);
				});

				$scope.carritomodal = function(id,tipo) { 

					$("#modalcarrito").modal();
                    $scope.carritoedit_arr = [];
                    $http.get('/resumencompra3/buscarmodalcarrito', {params: { id: id, tipo: tipo }}).
					success(function(data, status, headers, config){
					$scope.carritoedit_arr = data;

					$("#inputvalor_car"+id).val(data.cantidad);

					$('#inputvalor_car').prop('value',data.cantidad)
					  
					});

				}

				$scope.restarppv_car = function(id) { 

				   valor = $("#inputvalor_car"+id).val();

				   if (valor == 0) {
                      return false;
				   }

				   nuevovalor = parseFloat(valor) - parseFloat(1);
				   $("#inputvalor_car"+id).val(nuevovalor);
                     
				}

				$scope.sumarppv_car = function(id) { 
                    valor = $("#inputvalor_car"+id).val();
					nuevovalor = parseFloat(valor) + parseFloat(1);
					$("#inputvalor_car"+id).val(nuevovalor);
				}


				$scope.agregaralcarr_car = function(id) { 

					valor = $("#inputvalor_car"+id).val();

					if (valor == 0) {
                      return false;
				   }

                    $("#load_car"+id).show();
                    $("#cajacarris_car"+id).hide();
                    $("#checkcar_car"+id).show();

					$http.get('/resumencompra3/actualizarcarrito', {params: { id: id,cantidad:valor }}).
					success(function(data, status, headers, config){
						$("#load_car"+id).hide();
						$("#cajacarris_car"+id).show();

						$("#totalcarris_car").text(data);

							$scope.carritoprod = [];
							$http.get('/buscarcarritocheck').
							success(function(data, status, headers, config){
							$scope.carritoprod = data;
							});

						    $scope.productos_arr = [];
							$http.get('/buscarproductos').
							success(function(data, status, headers, config){
							$scope.productos_arr = data;
							});


							$http.get('/buscarcarrito').
							success(function(data, status, headers, config){
							   $("#totalcarris").text(data);
							});
						  
					});

					setTimeout(function() {
	                  $("#checkcar_car"+id).fadeOut(2000);
	                  $("#modalcarrito").modal('hide');
	                },1000);

				}



				$scope.restarppv_eliminar = function(id) { 

					valor = $("#inputvalor_car"+id).val();

					if (valor == 0) {
                      return false;
				   }

                    $("#load_car"+id).show();
                    $("#cajacarris_car"+id).hide();
                    $("#checkcar_car"+id).show();

					$http.get('/resumencompra3/eliminarcarrito', {params: { id: id,cantidad:valor }}).
					success(function(data, status, headers, config){
						$("#load_car"+id).hide();
						$("#cajacarris_car"+id).show();

						$("#totalcarris_car").text(data);

							$scope.carritoprod = [];
							$http.get('/buscarcarritocheck').
							success(function(data, status, headers, config){
							$scope.carritoprod = data;
							});

						    $scope.productos_arr = [];
							$http.get('/buscarproductos').
							success(function(data, status, headers, config){
							$scope.productos_arr = data;
							});


							$http.get('/buscarcarrito').
							success(function(data, status, headers, config){
							   $("#totalcarris").text(data);
							});
						  
					});

					setTimeout(function() {
	                  $("#checkcar_car"+id).fadeOut(2000);
	                  $("#modalcarrito").modal('hide');
	                },1000);

				}


		});

		