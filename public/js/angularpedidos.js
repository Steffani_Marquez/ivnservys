	
		var AllPost_app = angular.module('PedidosApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('PedidosController', function($scope, $http) {
    
            
            $scope.cambiar_status = function(id) { 
                //alert('sdfsf');
                $('.modal_status').modal('toggle');
                $('#pedido_id').val(id);
            }
            //$scope.editando_status(){

            //}

            $scope.cambiar_status_pago = function(id) { 
                $('.modal_status_pago').modal('toggle'); 
                $('#pedido_id').val(id);
            }            
            

            $scope.editando_pagos_status = function(){
                $http.get('/pedidos/changestatuspagos',{params: {id_trx:$('#pedido_id').val(), status_id: $('#status_pago').val() }}).
				success(function(data, status, headers, config){
                    $scope.estatus_obj = data;
                    $('.status_pago_table'+$('#pedido_id').val()).css('background-color',$scope.estatus_obj.color_rgb);
                    $('.status_pago_table'+$('#pedido_id').val()).text($scope.estatus_obj.status_name);
                    $('.modal_status_pago').modal('hide');
                    
				});
            }
            
            $scope.editando_status = function(){
                $http.get('/pedidos/changestatus',{params: {id_trx:$('#pedido_id').val(), status_id: $('#status').val() }}).
				success(function(data, status, headers, config){
                    $scope.estatus_obj = data;
                    //console.log($scope.estatus_obj);
                    //console.log($scope.color_rgb);
                    $('.status_table'+$('#pedido_id').val()).css('background-color',$scope.estatus_obj.color_rgb);
                    $('.status_table'+$('#pedido_id').val()).text($scope.estatus_obj.status_name);
                    $('.modal_status').modal('hide');
                    
				});
            }

		});