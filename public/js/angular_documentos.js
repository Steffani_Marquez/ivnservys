		
		var AllPost_app = angular.module('documentosApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('documentosController', function($scope, $http) {

 
			$scope.documentos_arr = [];
			$http.get('documentos/buscar', {params: { nombre:1}}).
			success(function(data, status, headers, config){
			$scope.documentos_arr = data;
			});

			$scope.buscar = function(){

				$scope.documentos_arr = [];
				$http.get('documentos/buscar', {params: { nombre: 1 }}).
				success(function(data, status, headers, config){
				$scope.documentos_arr = data;
				});
			}
			
			
			$scope.activardocuemnto = function(id){

				$http.get('documentos/activar', {params: { id: id }}).
				success(function(data, status, headers, config){
				$scope.documentos_arr = data;
				});
			}
			
			$scope.openmodalnuevo = function(){
				$('#openmodalnuevo').modal('toggle');
				
			}

			$scope.tipodocchange = function(){

				if($('#tipo_documento').val() == 1){
					$('.fecha_vencimiento').removeAttr('hidden');
				}else{
					$('.fecha_vencimiento').val('');
					$('.fecha_vencimiento').attr('hidden','hidden');
				}
					
			}

	});