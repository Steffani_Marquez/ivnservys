<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'documento';
    protected $fillable = ['url_documento','nombre_documento','extension_doc','fecha_vencimiento','tipo_documento','id_cliente','aprobado','fecha_registro','habilitado'];

}
