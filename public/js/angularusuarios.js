	
		var AllPost_app = angular.module('UsuariosApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('UsuariosController', function($scope, $http) {
    
            
            $scope.cambiar_status = function(id) { 
                //alert('sdfsf');
                $('.modal_status').modal('toggle');
                $('#usuario_id').val(id);
            }
            //$scope.editando_status(){

            //}

            $scope.cambiar_usuario = function(id) { 
                $('.modal_editar_producto').modal('toggle'); 
                $('#user_id').val(id);
                
                $http.get('/usuario/user',{params: {user_id:id }}).
				success(function(data, status, headers, config){
                    $scope.user = data;    
				});
                
            }            
            
  
            $scope.editando_status = function(){
                $http.get('/usuario/changestatus',{params: {user_id:$('#usuario_id').val(), status: $('#status').val() }}).
				success(function(data, status, headers, config){
                    $scope.estatus_obj = data;
                    //console.log($scope.estatus_obj);
                    //console.log($scope.color_rgb);
                    if($scope.estatus_obj.active == 1){
                        $('.status_table'+$('#usuario_id').val()).css('background-color','#28a745');
                        $('.status_table'+$('#usuario_id').val()).text('ACTIVO');

                    }else{
                        $('.status_table'+$('#usuario_id').val()).css('background-color','#dc3545');
                        $('.status_table'+$('#usuario_id').val()).text('INACTIVO');
                    
                    }
                    $('.modal_status').modal('hide');
                    
				});
            }

            $scope.eliminar = function(id) { 
                $('.modal_eliminar_user').modal('toggle'); 
                $('#cliente_id').val(id);
                
            }                 

            $scope.eliminarcliente = function(){
                $http.get('/usuario/eliminar',{params: { cliente_id: $('#cliente_id').val() }}).
				success(function(data, status, headers, config){
                    window.location = '/usuario/list-clients';    
				});
                                
            }

		});