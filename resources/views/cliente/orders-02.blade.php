@extends('cliente.cliente-layout')
@section('content')
@section('title', 'Mis pedidos')
@section('enlace1', 'Home')
@section('enlace2', 'Mis Pedidos')
<section class="py-5" ng-module="ordenesApp" ng-controller="ordenesController">
    <div class="container py-4">
        <div class="row gy-5">
            <div class="col-lg-12">
                <p class="lead text-muted mb-5">Si tiene una inquietud contacte a su asesor de venta</p>
                <div class="table-responsive">
                <table class="table text-nowrap">
                    <thead>
                    <tr class="text-sm">
                    <th class="">#</th>
                        <th class="">Fecha Solicitud</th>
                        <th class="">Estatus</th>
                        <th class="">Eliminar</th>
                        <th class="">Detalle</th>
                      </tr>                     
                    </tr>
                    </thead>
                        <tbody>
                        @foreach($datos as $dato)
                        <tr class="text-sm">
                            <th class="align-middle py-3"># 1735</th>
                            <td class="align-middle py-3">22/06/2013</td>
                            <td class="align-middle py-3"><span class="badge  fw-light text-uppercase bg-info">Pendiente</span></td>
                            <td class="" ng-click="openmodalemilinar(1)"><i class="fa fa-trash" style="font-size: 19px;color: #d9534f;"></i></td>
                            <td class=""><a href="orden-detalle-cliente?id=216&datos={{json_encode($datos)}}">  <i class="fa fa-eye"  style="font-size: 19px;color: #b3b3b3;"></a></td>
                        </tr>
                        @endforeach    
                        <tr  ng-repeat="ord in ordenes_arr|filter:buscador">
                            <td class=""># <% ord.id %></td>
                            <td class=""><% ord.fecha_pedido %></td>
                            <td class="">
                            <span class="badge fw-light text-uppercase bg-info" ng-if="ord.status == 'Pendiente'"><% ord.status %></span>
                            <span class="badge fw-light text-uppercase  bg-warning " ng-if="ord.status == 'Aprobado'"><% ord.status %></span>
                            <span class="badge fw-light text-uppercase  bg-success " ng-if="ord.status == 'Facturado'"><% ord.status %></span>
                            <span class="badge fw-light text-uppercase  bg-danger " ng-if="ord.status == 'Cancelado'"><% ord.status %></span>
                            </td>
                            <td class="" ng-click="openmodalemilinar()"><i class="fa fa-trash" style="font-size: 19px;color: #d9534f;"></i></td>
                            <td class=""><a href="orden-detalle-cliente?id=<% ord.id %>&datos={{json_encode($datos)}}"><i class="fa fa-eye"  style="font-size: 19px;color: #b3b3b3;"></a></td>
                            
                        </tr>
                        </tbody>
                    <tfoot>

                    </tfoot>
                </table>
                </div>
            </div>
        </div>
    </div>
</section>


   <style type="text/css">
     .table-inbox tr td {
        padding: 5px !important;
      }
   </style>

    <section id="main-content" ng-module="ordenesApp" ng-controller="ordenesController">

  
      
      <div class="modal " id="openmodaleliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel">Eliminar orden</h4>
              <button type="button" class="btn-close" ata-bs-dismiss="modal" aria-label="Close"></button>

            </div>
            <div class="modal-body">
              <h4>¿Deseas Eliminar esta orden?</h4>
              <input type="hidden" id="orden_id" value="">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
              <button type="button" class="btn btn-primary" ng-click="eliminarorden()">SI</button>
            </div>
          </div>
        </div>
      </div>      


    </section>

@stop       
@section('script')
<script src="{{url('js/angular_ordenes_02.js')}}"></script>
@stop
