	
		var AllPost_app = angular.module('AdsalvavidasApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('SalvavidasController', function($scope, $http) {
		
				
				$scope.pagos = [];
                console.log($('#trx_id').val());
				$http.get('pagar/buscarpagos',{params: {'id_trx': $('#id_trx').val()}}).
				success(function(data, status, headers, config){
				$scope.pagos = data;
				});

				$scope.pagos = [];
                $scope.eliminarpago = function(){
                    
                    $http.get('pagar/eliminarpago',{params: {'id': $('#id_pago').val(),'id_trx': $('#id_trx').val()}}).
                    success(function(data, status, headers, config){
                    $scope.pagos = data; 
                        $('.eliminarpago').modal('hide'); 
                        window.location = "/pagar?ad="+$('#id_trx').val();
                    });
                }
                
                $scope.openmodaleliminar = function(id){
                    console.log('sadd')
                    $('#id_pago').val(id);
                    $('.eliminarpago').modal('toggle'); 
                }
                
                $scope.openmodalcupones = function(id){
                    $('.modalCupon').modal('toggle'); 
                    
                    
                     
                }
                $scope.aplicarcupon = function(cupon_id){
                    $http.get('pagar/aplicar-cupon',{params: {'cupon_id': cupon_id,'id_trx': $('#id_trx').val()}}).
                    success(function(data, status, headers, config){
                    if(data == 'usado'){
                        $('.alert-apply-cupon').text('Este pedido ya ha usado Cupón');
                        $('.alert-apply-cupon').removeAttr('hidden');
                        $('.modalCupon').modal('hide'); 
                    }else{
                        $scope.pagos = data; 
                        $('.modalCupon').modal('hide'); 
                        window.location = "/pagar?ad="+$('#id_trx').val();
                    }

                        
                    });                    
                }
                
                

        }); 